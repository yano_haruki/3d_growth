﻿using SlimDX;

namespace WindowsFormsApp1
{

	public class MyGame
	{
		public void Setup()
		{
			// Skybox
			var skyboxRenderer			= new GameObject().AddComponent<MeshRenderer>();
			skyboxRenderer.Mesh			= Mesh.CreateSphere();
			skyboxRenderer.Transform.LocalScale = new Vector3(10000, 10000, 10000);
			skyboxRenderer.Material		= new Material(new Shader(Properties.Resources.shader_skybox));
			skyboxRenderer.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));

			// ライト
			var light							= new GameObject().AddComponent<DirectionalLight>();
			light.Color							= new Color4(1.0f, 1.0f, 1.0f, 1.0f);
			light.Transform.LocalEulerAngles	= new Vector3(50 * Mathf.Deg2Rad, 0 * Mathf.Deg2Rad, 0);

			// UIカメラを作る
			var uiCamera			= new GameObject().AddComponent<UICamera>();
			uiCamera.ClearFlag		= CameraClearFlags.DontClear;

			// カメラ
			var camera				= new GameObject().AddComponent<PerspectiveCamera>();
			Camera.Main		= camera;
			camera.Transform.LocalPosition = new Vector3(0, 0, -10);
			camera.SetupGizmoCamera();
			// カメラにFPSCameraControllerをアタッチ
			//camera.GameObject.AddComponent<FpsCameraController>();
			camera.GameObject.AddComponent<LookCenterCameraController>().Setup();
			skyboxRenderer.Transform.Parent = camera.Transform;

			//FBX
			var fbxMesh = FBXManager.Instance.CreateMeshList("space_ship.fbx")[0];
			//var fbxMesh = FBXManager.Instance.CreateMeshList("untitled.fbx")[0];
			//var fbxMesh = Mesh.CreateSphere();
			var renderer = new GameObject().AddComponent<MeshRenderer>();
			renderer.Mesh			= fbxMesh;
			renderer.Transform.LocalPosition = new Vector3(-2, 0, 2);
			renderer.Transform.LocalScale = new Vector3(0.001f, 0.001f, 0.001f);
			//renderer.Material		= new Material(new Shader(Properties.Resources.shader_build_shadow_map));
			renderer.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap_normalmap));
			renderer.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.space_ship_d));
			renderer.Material.SetTexture("_NormalMap", new Texture2D(Properties.Resources.space_ship_n));
			renderer.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer.Material.SetFloat("_Shininess", 50);
			renderer.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer.Material.SetFloat("_Metalness", 0.12f);
			renderer.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer.Material.SetFloat("_Roughness", 0.14f);

			// Plane
			var renderer1			= new GameObject().AddComponent<MeshRenderer>();
			renderer1.Mesh			= Mesh.CreatePlane(1, 1, 10, 10);
			renderer1.Transform.LocalPosition = new Vector3(0, 1, 0);
			renderer1.Transform.LocalScale = new Vector3(1, 1, 1);
			renderer1.Transform.LocalEulerAngles = new Vector3(90 * Mathf.Deg2Rad, 180 * Mathf.Deg2Rad, 0);
			renderer1.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer1.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_red));
			renderer1.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer1.Material.SetFloat("_Shininess", 50);
			renderer1.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer1.Material.SetFloat("_Metalness", 0.12f);
			renderer1.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer1.Material.SetFloat("_Roughness", 0.14f);

			// Sphere
			var renderer2			= new GameObject().AddComponent<MeshRenderer>();
			renderer2.Mesh			= Mesh.CreateSphere();
			renderer2.Transform.LocalPosition = new Vector3(2.5f, 2, -0.5f);
			renderer2.Transform.LocalScale = new Vector3(1, 1, 1);
			renderer2.Transform.LocalEulerAngles = new Vector3(90 * Mathf.Deg2Rad, 0, 0);
			renderer2.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer2.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_green));
			renderer2.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer2.Material.SetFloat("_Shininess", 50);
			renderer2.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer2.Material.SetFloat("_Metalness", 0.12f);
			renderer2.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer2.Material.SetFloat("_Roughness", 0.14f);

			// Sphere
			var renderer3			= new GameObject().AddComponent<MeshRenderer>();
			renderer3.Mesh			= Mesh.CreateSphere();
			renderer3.Transform.LocalPosition = new Vector3(2.75f, 4, -2.0f);
			renderer3.Transform.LocalScale = new Vector3(1, 1, 1);
			renderer3.Transform.LocalEulerAngles = new Vector3(90 * Mathf.Deg2Rad, 0, 0);
			renderer3.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer3.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_blue));
			renderer3.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer3.Material.SetFloat("_Shininess", 50);
			renderer3.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer3.Material.SetFloat("_Metalness", 0.12f);
			renderer3.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer3.Material.SetFloat("_Roughness", 0.14f);

			// Plane
			var renderer4			= new GameObject().AddComponent<MeshRenderer>();
			renderer4.Mesh			= Mesh.CreatePlane(20, 20, 10, 10);
			renderer4.Transform.LocalPosition = new Vector3(0, -0.9f, 0);
			renderer4.Transform.LocalScale = new Vector3(1, 1, 1);
			renderer4.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer4.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_white));
			renderer4.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer4.Material.SetFloat("_Shininess", 50);
			renderer4.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer4.Material.SetFloat("_Metalness", 0.12f);
			renderer4.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer4.Material.SetFloat("_Roughness", 0.14f);



			// デバッグ表示
			// テキスト背景
			var imageRenderer				= new GameObject().AddComponent<UIImageRenderer>();
			imageRenderer.ViewportPosition	= new Vector2(0f, 0f);
			imageRenderer.ViewportSize		= new Vector2(1.0f, 0.1f);
			imageRenderer.Texture			= new Texture2D(Properties.Resources.tex_black);
			imageRenderer.TintColor			= new Color4(0.5f, 1, 1f, 1f);
			// テキスト
			var textRenderer				= new GameObject().AddComponent<UITextRenderer>();
			textRenderer.ViewportPosition	= new Vector2(0f, 0.03f);
			textRenderer.Color				= new Color4(1, 1, 1f, 1f);
			textRenderer.Size				= 0.5f;
			textRenderer.Text				= " ";
			textRenderer.HorizontalPivot	= UITextRenderer.HorizontalPivotType.Left;
			textRenderer.VerticalPivot		= UITextRenderer.VerticalPivotType.Bottom;




			// FBX表示
			/*
			var fbxMesh = FBXManager.Instance.CreateMeshList("fbx_growth_mo.fbx")[0];
			var renderer = new GameObject().AddComponent<MeshRenderer>();
			renderer.Mesh			= fbxMesh;
			renderer.Transform.LocalPosition = new Vector3(0, -100, 0);
			renderer.Transform.LocalScale = new Vector3(1, 1, 1);
			renderer.Material		= new Material(new Shader(Properties.Resources.shader_unlit));
			renderer.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_fbx_growth));
			renderer.Material.SetColor("_Tint", new Color4(1,1,1));
			_skinnedMesh = fbxMesh;
			*/

			// CapsuleCollider
			/*
			// 非金属表現
			var renderer1			= new GameObject().AddComponent<MeshRenderer>();
			var collider = renderer1.GameObject.AddComponent<CapsuleCollider>();
			renderer1.Mesh			= Mesh.CreateCapsule();
			renderer1.Transform.LocalPosition = new Vector3(0, 0, 0);
			renderer1.Transform.LocalScale = new Vector3(2, 2, 2);
			renderer1.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer1.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_white));
			renderer1.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer1.Material.SetFloat("_Shininess", 50);
			renderer1.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer1.Material.SetFloat("_Metalness", 0.12f);
			renderer1.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer1.Material.SetFloat("_Roughness", 0.14f);

			// 非金属表現
			var renderer2			= new GameObject().AddComponent<MeshRenderer>();
			renderer2.GameObject.AddComponent<CameraBaseActorMover>().Setup(camera.Transform);
			var detector = renderer2.GameObject.AddComponent<CapsuleCollisionDetector>();
			detector.IsDebug = true;
			renderer2.Mesh			= Mesh.CreateCapsule();
			renderer2.Transform.LocalPosition = new Vector3(-3, 0, 0);
			renderer2.Transform.LocalScale = new Vector3(1, 1, 1);
			renderer2.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer2.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_white));
			renderer2.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer2.Material.SetFloat("_Shininess", 50);
			renderer2.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer2.Material.SetFloat("_Metalness", 0.12f);
			renderer2.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer2.Material.SetFloat("_Roughness", 0.14f);
			detector.Targets.Add(collider);
			*/



			//AxisAlignedBoundingBoxCollider
			/*
			 *
			// 非金属表現
			var renderer1			= new GameObject().AddComponent<MeshRenderer>();
			var collider = renderer1.GameObject.AddComponent<AxisAlignedBoundingBoxCollider>();
			collider.Size = new Vector3(1,1,1);
			renderer1.Mesh			= Mesh.CreateCube();
			renderer1.Transform.LocalPosition = new Vector3(0, 0, 0);
			renderer1.Transform.LocalScale = new Vector3(2, 2, 2);
			renderer1.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer1.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_white));
			renderer1.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer1.Material.SetFloat("_Shininess", 50);
			renderer1.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer1.Material.SetFloat("_Metalness", 0.12f);
			renderer1.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer1.Material.SetFloat("_Roughness", 0.14f);

			// 非金属表現
			var renderer2			= new GameObject().AddComponent<MeshRenderer>();
			renderer2.GameObject.AddComponent<CameraBaseActorMover>().Setup(camera.Transform);
			var detector = renderer2.GameObject.AddComponent<AxisAlignedBoundingBoxCollisitionDetector>();
			detector.Size = new Vector3(1,1,1);
			detector.IsDebug = true;
			renderer2.Mesh			= Mesh.CreateCube();
			renderer2.Transform.LocalPosition = new Vector3(-3, 0, 0);
			renderer2.Transform.LocalScale = new Vector3(1, 1, 1);
			renderer2.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer2.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_white));
			renderer2.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer2.Material.SetFloat("_Shininess", 50);
			renderer2.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer2.Material.SetFloat("_Metalness", 0.12f);
			renderer2.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer2.Material.SetFloat("_Roughness", 0.14f);
			detector.Targets.Add(collider);
			 * */



			/*
			// テキストテンプレ
			var textRenderer				= new GameObject().AddComponent<UITextRenderer>();
			textRenderer.ViewportPosition	= new Vector2(0f, 0f);
			textRenderer.Color				= new Color4(1, 1, 1f, 1f);
			textRenderer.Size				= 0.5f;
			textRenderer.Text				= "ABCDEFGHIJKLMNOP";
			textRenderer.HorizontalPivot	= UITextRenderer.HorizontalPivotType.Left;
			textRenderer.VerticalPivot		= UITextRenderer.VerticalPivotType.Bottom;
			*/


			/*
			// UIテンプレ
			var imageRenderer				= new GameObject().AddComponent<UIImageRenderer>();
			imageRenderer.ViewportPosition	= new Vector2(0.5f, 0.5f);
			imageRenderer.Texture			=  new Texture2D(Properties.Resources.tex_white);
			imageRenderer.TintColor			= new Color4(1, 1, 1f, 1f);
			*/

			/*
			// 金属表現のテンプレ
			var renderer2			= new GameObject().AddComponent<MeshRenderer>();
			renderer2.Mesh			= Mesh.CreateSphere();
			renderer2.Transform.LocalPosition = new Vector3(1, 0, 0);
			renderer2.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer2.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_white));
			renderer2.Material.SetColor("_Tint", new Color4(0.8f, 0f, 0f));
			renderer2.Material.SetFloat("_Shininess", 50);
			renderer2.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer2.Material.SetFloat("_Metalness", 1.0f);
			renderer2.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer2.Material.SetFloat("_Roughness", 0.123f);
			_renderers.Add(renderer2);
			*/


			// 非金属表現のテンプレ
			/*
			var renderer1			= new GameObject().AddComponent<MeshRenderer>();
			renderer1.GameObject.AddComponent<CameraBaseActorMover>().Setup(camera.Transform);
			renderer1.GameObject.AddComponent<SphereCollider>();
			renderer1.Mesh			= Mesh.CreateSphere();
			renderer1.Transform.LocalPosition = new Vector3(0, 0, 0);
			renderer1.Transform.LocalScale = new Vector3(2, 2, 2);
			renderer1.Material		= new Material(new Shader(Properties.Resources.shader_lighting_cubemap));
			renderer1.Material.SetTexture("_MainTex", new Texture2D(Properties.Resources.tex_white));
			renderer1.Material.SetColor("_Tint", new Color4(1,1,1));
			renderer1.Material.SetFloat("_Shininess", 50);
			renderer1.Material.SetCubeMap("_CubeMap", new TextureCube(Properties.Resources.cube_sky_01, 512));
			renderer1.Material.SetFloat("_Metalness", 0.12f);
			renderer1.Material.SetFloat("_IndirectDiffRefl", 0.66f);
			renderer1.Material.SetFloat("_Roughness", 0.14f);
			*/

		}

		public void Update()
		{
			// レイキャスト
			/*
			if (Input.Instance.GetMouseDown(Input.MouseKeys.Left)) {
				foreach (var col in ComponentManager.Instance.GetComponents<ICollider>()) {
					var renderer = (col as Component).GameObject.GetComponent<MeshRenderer>();
					renderer.Material.SetColor("_Tint", new Color4(1,1,1));
				}

				var ray = Camera.Main.ViewportPointToRay(Input.Instance.ViewportMousePosition);
				ICollider collider;
				if (Physics.RaycastSphere(ray.Origin, ray.Direction, out collider)) {
					if (collider != null) {
						var renderer = (collider as Component).GameObject.GetComponent<MeshRenderer>();
						renderer.Material.SetColor("_Tint", new Color4(1,0,0));
					}
				}

			}
			*/

			// スキンメッシュアニメーション
			/*
			if (Input.Instance.GetKeyDown(System.Windows.Forms.Keys.A)) {
				_currentAnimationFrame = 0;
				_doAnimate = true;
			}

			if (_currentAnimationFrame < 119) {
				if (_doAnimate) {
					_currentAnimationFrame++;
				}
			}
			else {
				_doAnimate = false;
			}
			_skinnedMesh.FrameCount = _currentAnimationFrame;
			*/

			ComponentManager.Instance.Update();

		}

		public void LateUpdate()
		{
		}

		public void Draw()
		{
			ComponentManager.Instance.Draw();
			ComponentManager.Instance.DrawGizmos();
			ComponentManager.Instance.PostDraw();
		}
	}
}
