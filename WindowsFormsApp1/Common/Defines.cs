﻿using SlimDX;

namespace WindowsFormsApp1
{
	public static class Defines
	{
		public const string SHADER_PROPERTY_NAME_WORLD_LIGHT_POS = "_WorldSpaceLightPos";
		public const string SHADER_PROPERTY_NAME_LIGHT_COLOR = "_LightColor";
	}

	public enum FontType
	{
		Gothic,
		Mincho
	}

	public enum RenderQueue
	{
		Depth			= 0,
		Geometry		= 1,
		Transparent		= 5
	}

	public class Rect
	{
		// x: minX, y: minY, z: maxX, w: maxY
		private Vector4 _positions			= new Vector4();
		public float Width
		{
			get { return _positions.Z - _positions.X; }
			set { _positions		= new Vector4(_positions.X, _positions.Y, _positions.X + value, _positions.W); }
		}
		public float Height
		{
			get { return _positions.W - _positions.Y; }
			set { _positions		= new Vector4(_positions.X, _positions.Y, _positions.Z, _positions.Y + value); }
		}
		public Vector2 Size
		{
			get { return new Vector2(Width, Height); }
			set
			{
				Width		= value.X;
				Height		= value.Y;
			}
		}
		public float MinX
		{
			get { return _positions.X; }
			set { _positions		= new Vector4(value, _positions.Y, value + Width, _positions.W); }
		}
		public float MinY
		{
			get { return _positions.Y; }
			set { _positions		= new Vector4(_positions.X, value, _positions.Z, value + Height); }
		}
		public float MaxX
		{
			get { return _positions.Z; }
			set { _positions		= new Vector4(value - Width, _positions.Y, value, _positions.W); }
		}
		public float MaxY
		{
			get { return _positions.W; }
			set { _positions		= new Vector4(_positions.X, value - Height, _positions.Z, value); }
		}
		public Vector2 Min
		{
			get { return new Vector2(_positions.X, _positions.Y); }
			set { _positions		= new Vector4(value.X, value.Y, value.X + Width, value.X + Height); }
		}
		public Vector2 Max
		{
			get { return new Vector2(_positions.Z, _positions.W); }
			set { _positions		= new Vector4(value.X - Width, value.Y - Height, value.X, value.Y); }
		}
		public Vector2 Center
		{
			get { return new Vector2(_positions.X + (_positions.Z - _positions.X) * 0.5f, _positions.Y + (_positions.W - _positions.Y) * 0.5f); }
			set {
				_positions			= new Vector4(value.X - Width * 0.5f, value.Y - Height * 0.5f, value.X + Width * 0.5f, value.Y + Height * 0.5f);
			}
		}

		public override string ToString()
		{
			return _positions.ToString();
		}
	}

	public struct Ray
	{
		public Vector3 Origin { get; set; }
		public Vector3 Direction { get; set; }
	}
}
