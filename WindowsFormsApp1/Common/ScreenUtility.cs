﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	class ScreenUtility
	{
		public static int Width { get; set; }
		public static int Height { get; set; }
		public static Vector2 Size { get { return new Vector2(Width, Height); } }
	}
}
