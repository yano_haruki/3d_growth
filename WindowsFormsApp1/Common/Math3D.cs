﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{

	public static class Math3D
	{
		#region distance

		/// <summary>
		/// 点と直線との距離を求める
		/// http://marupeke296.com/COL_3D_No27_CapsuleCapsule.html
		/// </summary>
		public static float PointLineDistance(Vector3 point, Vector3 linePoint, Vector3 lineDir)
		{
			Vector3 h;
			float t;
			return PointLineDistance(point, linePoint, lineDir, out h, out t);
		}
		public static float PointLineDistance(Vector3 point, Vector3 linePoint, Vector3 lineDir, out Vector3 h, out float t)
		{
			// 点から直線に降ろした垂線と直線との交点
			h = ProjectPointOnLine(point, linePoint, lineDir, out t);
			// 垂線の長さ
			return (h - point).Length();
		}

		/// <summary>
		/// 点と線分との距離を求める
		/// http://marupeke296.com/COL_3D_No27_CapsuleCapsule.html
		/// </summary>
		public static float PointSegmentDistance(Vector3 point, Vector3 segmentStartPoint, Vector3 segmentEndPoint, out Vector3 h, out float t)
		{
			var distance = PointLineDistance(point, segmentStartPoint, segmentEndPoint - segmentStartPoint, out h, out t);
			if (!IsSharpAngle(point, segmentStartPoint, segmentEndPoint)) {
				// pointからsegmentに降ろした垂線とsegmentとの交点がStartPoint側の線分外にある場合
				h = segmentStartPoint;
				return (segmentStartPoint - point).Length();
			}
			else if(!IsSharpAngle(point, segmentEndPoint, segmentStartPoint)) {
				// pointからsegmentに降ろした垂線とsegmentとの交点がEndPoint側の線分外にある場合
				h = segmentEndPoint;
				return (segmentEndPoint - point).Length();
			}
			return distance;
		}

		/// <summary>
		/// 直線と直線の最短距離を求める
		/// http://marupeke296.com/COL_3D_No27_CapsuleCapsule.html
		/// </summary>
		public static float LineLineDistance(Vector3 linePoint1, Vector3 lineVector1, Vector3 linePoint2, Vector3 lineVector2)
		{
			var isParallel = IsParallel(lineVector1, lineVector2);

			if (isParallel) {
				// 点と線の最短距離を求めればOK
				var distance = PointLineDistance(linePoint1, linePoint2, lineVector2);
				return distance;
			}
			else {
				// 平行じゃない場合
				var dotVec1Vec2 = Vector3.Dot(lineVector1, lineVector2);
				var dotVec1Vec1 = lineVector1.LengthSquared();
				var dotVec2Vec2 = lineVector2.LengthSquared();
				var p21p11 = linePoint1 - linePoint2;
				var t1 = dotVec1Vec2 * Vector3.Dot(lineVector2, p21p11) - dotVec2Vec2 * Vector3.Dot(lineVector1, p21p11) / (dotVec1Vec1 * dotVec2Vec2 - dotVec1Vec2 * dotVec1Vec2);
				var p1 = linePoint1 + lineVector1 * t1;
				var t2 = Vector3.Dot(lineVector2, (p1 - linePoint2)) / dotVec2Vec2;
				var p2 = linePoint2 + lineVector2 * t2;
				return (p2 - p1).Length();
			}
		}
		public static float LineLineDistance(Vector3 linePoint1, Vector3 lineVector1, Vector3 linePoint2, Vector3 lineVector2, out Vector3 p1, out Vector3 p2, out float t1, out float t2)
		{
			var isParallel = IsParallel(lineVector1, lineVector2);

			if (isParallel) {
				// 点と線の最短距離を求めればOK
				var distance = PointLineDistance(linePoint1, linePoint2, lineVector2, out p2, out t2);
				p1 = linePoint1;
				t1 = 0.0f;
				return distance;
			}
			else {
				// 平行じゃない場合
				var dotVec1Vec2 = Vector3.Dot(lineVector1, lineVector2);
				var dotVec1Vec1 = lineVector1.LengthSquared();
				var dotVec2Vec2 = lineVector2.LengthSquared();
				var p21p11 = linePoint1 - linePoint2;
				t1 = dotVec1Vec2 * Vector3.Dot(lineVector2, p21p11) - dotVec2Vec2 * Vector3.Dot(lineVector1, p21p11) / (dotVec1Vec1 * dotVec2Vec2 - dotVec1Vec2 * dotVec1Vec2);
				p1 = linePoint1 + lineVector1 * t1;
				t2 = Vector3.Dot(lineVector2, (p1 - linePoint2)) / dotVec2Vec2;
				p2 = linePoint2 + lineVector2 * t2;
				return (p2 - p1).Length();
			}
		}
	
		/// <summary>
		/// 線分と線分の最短距離を求める
		/// http://marupeke296.com/COL_3D_No27_CapsuleCapsule.html
		/// </summary>
		public static float SegmentSegmentDistance(Vector3 segmentStartPoint1, Vector3 segmentEndPoint1, Vector3 segmentStartPoint2, Vector3 segmentEndPoint2)
		{
			Vector3 p1;
			Vector3 p2;
			float t1;
			float t2;
			return SegmentSegmentDistance(segmentStartPoint1, segmentEndPoint1, segmentStartPoint2, segmentEndPoint2, out p1, out p2, out t1, out t2);
		}
		public static float SegmentSegmentDistance(Vector3 segmentStartPoint1, Vector3 segmentEndPoint1, Vector3 segmentStartPoint2, Vector3 segmentEndPoint2, out Vector3 p1, out Vector3 p2, out float t1, out float t2)
		{
			var vector1 = segmentEndPoint1 - segmentStartPoint1;
			var vector2 = segmentEndPoint2 - segmentStartPoint2;
			float distance;
		
			if (vector1.Length() < float.Epsilon) {
				// 線分1が縮退
				if (vector2.Length() < float.Epsilon) {
					// 線分2も縮退
					distance = (segmentStartPoint2 - segmentStartPoint1).Length();
					p1 = segmentStartPoint1;
					p2 = segmentStartPoint2;
					t1 = 0.0f;
					t2 = 0.0f;
					return distance;
				}
				else {
					// 線分2は縮退してない
					distance = PointSegmentDistance(segmentStartPoint1, segmentStartPoint2, segmentEndPoint2, out p2, out t2);
					p1 = segmentStartPoint1;
					t1 = 0.0f;
					return distance;
				}
			}
			else if (vector2.Length() < float.Epsilon) {
				// 線分2のみ縮退
				distance = PointSegmentDistance(segmentStartPoint2, segmentStartPoint1, segmentEndPoint1, out p1, out t1);
				p2 = segmentStartPoint2;
				t1 = Mathf.Clamp01(t1);
				t2 = 0.0f;
				return distance;
			}

			/*** 線分同士 ***/

			// 2線分が平行だったら垂線の端点の一つをP1に仮決定
			if (IsParallel(vector1, vector2)) {
				t1 = 0.0f;
				p1 = segmentStartPoint1;
				distance = PointSegmentDistance(segmentStartPoint1,segmentStartPoint2, segmentEndPoint2, out p2, out t2);
				if (0.0f <= t2 && t2 <= 1.0f) {
					return distance;
				}
			}
			else {
				// 線分はねじれの関係
				// 2直線間の最短距離を求めて仮のt1,t2を求める
				var disance = LineLineDistance(segmentStartPoint1, vector1, segmentStartPoint2, vector2, out p1, out p2, out t1, out t2);
				if (0.0f <= t1 && t1 <= 1.0f && 0.0f <= t2 && t2 <= 1.0f) {
					return disance;
				}
			}
		
			// 垂線の足が外にある事が判明
			// S1側のt1を0～1の間にクランプして垂線を降ろす
			t1 = Mathf.Clamp01(t1);
			p1 = segmentStartPoint1 + vector1 * t1;
			distance = PointSegmentDistance(p1, segmentStartPoint2, segmentEndPoint2, out p2, out t2);
			if (0.0f <= t2 && t2 <= 1.0f) {
				return distance;
			}

			// S2側が外だったのでS2側をクランプ、S1に垂線を降ろす
			t2 = Mathf.Clamp01(t2);
			p2 = segmentStartPoint2 + vector2 * t2;
			distance = PointSegmentDistance(p2, segmentStartPoint1, segmentEndPoint1, out p1, out t1);
			if (0.0f <= t1 && t1 <= 1.0f) {
				return distance;
			}

			// 双方の端点が最短と判明
			t1 = Mathf.Clamp01(t1);
			p1 = segmentStartPoint1 + vector1 * t1;
			return (p2 - p1).Length();
		}
		#endregion

		#region projection

		/// <summary>
		/// 点から直線に降ろした垂線と直線との交点を求める
		/// </summary>
		public static Vector3 ProjectPointOnLine(Vector3 point, Vector3 linePoint, Vector3 lineDir)
		{
			float t;
			return ProjectPointOnLine(point, linePoint, lineDir, out t);
		}

		/// <summary>
		/// 点から直線に降ろした垂線と直線との交点を求める
		/// </summary>
		public static Vector3 ProjectPointOnLine(Vector3 point, Vector3 linePoint, Vector3 lineDir, out float t)
		{
			// lineStartPointからlineDir * tが交点を示すときのtの値
			t = Vector3.Dot(lineDir, point - linePoint) / lineDir.LengthSquared();
			// 点から直線に降ろした垂線との交点
			return linePoint + t * lineDir;
		}
	
		/// <summarys>
		/// 点から線分に降ろした垂線と線分の好転を求める
		/// 垂線が線分上に降ろせない場合にはクランプされる
		/// </summary>
		/// <returns></returns>
		public static Vector3 ProjectPointOnSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point)
		{
			var vector = linePoint2 - linePoint1;
			var projectedPoint = ProjectPointOnLine(linePoint1, vector.Normalized(), point);
			var side = PointOnWhichSideOfSegment(linePoint1, linePoint2, projectedPoint);

			//The projected point is on the line segment
			if (side == 0)
			{
				return projectedPoint;
			}

			if (side == 1)
			{
				return linePoint1;
			}

			if (side == 2)
			{
				return linePoint2;
			}

			//output is invalid
			return Vector3.Zero;
		}

		/// <summary>
		/// 点から平面上に降ろした垂線と平面との交点を求める
		/// </summary>
		public static Vector3 ProjectPointOnPlane(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
		{

			float distance;
			Vector3 translationVector;

			//First calculate the distance from the point to the plane:
			distance = SignedDistancePlanePoint(planeNormal, planePoint, point);

			//Reverse the sign of the distance
			distance *= -1;

			//Get a translation vector
			translationVector = planeNormal.Normalized() * distance;

			//Translate the point to form a projection
			return point + translationVector;
		}

		#endregion

		#region intersection

		/// <summary>
		/// 直線と平面の交点を求める
		/// 直線と平面が平行な場合（交点が求められない場合）にはfalseを返す
		/// </summary>
		public static bool LinePlaneIntersection(out Vector3 intersection, Vector3 linePoint, Vector3 lineDir, Vector3 planeNormal, Vector3 planePoint)
		{
			// linePointと、直線と平面との交点との距離を求める
			var dotNumerator = Vector3.Dot((planePoint - linePoint), planeNormal);
			var dotDenominator = Vector3.Dot(lineDir, planeNormal);
		
			if (dotDenominator != 0.0f)
			{
				// 直線と平面が平行でない場合（交点が求められる場合）

				var length = dotNumerator / dotDenominator;
				var vector = lineDir.Normalized() * length;
				intersection = linePoint + vector;

				return true;
			}
			else
			{
				// 直線と平面が平行で交点が求められない場合
				intersection = Vector3.Zero;
				return false;
			}
		}

		/// <summary>
		/// 直線と直線の交点を求める
		/// 求められたらtrue
		/// </summary>
		public static bool LineLineIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
		{

			intersection = Vector3.Zero;

			var lineVec3 = linePoint2 - linePoint1;
			var crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
			var crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

			var planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

			//Lines are not coplanar. Take into account rounding errors.
			if ((planarFactor >= 0.00001f) || (planarFactor <= -0.00001f))
			{
				return false;
			}

			//Note: sqrMagnitude does x*x+y*y+z*z on the input vector.
			float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.LengthSquared();

			if ((s >= 0.0f) && (s <= 1.0f))
			{
				intersection = linePoint1 + (lineVec1 * s);
				return true;
			}

			else
			{
				return false;
			}
		}
		
		/// <summary>
		/// 直線と球体の交点を求める
		/// 求められたらtrue
		/// http://marupeke296.com/COL_3D_No24_RayToSphere.html
		/// </summary>
		public static bool LineSphereIntersection(Vector3 linePoint, Vector3 lineVec, Vector3 sphereCenter, float sphereRadius)
		{
			var intersection1 = Vector3.Zero;
			var intersection2 = Vector3.Zero;
			return LineSphereIntersection(linePoint, lineVec, sphereCenter, sphereRadius, out intersection1, out intersection2);
		}
		public static bool LineSphereIntersection(Vector3 linePoint, Vector3 lineVec, Vector3 sphereCenter, float sphereRadius, out Vector3 intersection1, out Vector3 intersection2)
		{
			intersection1 = Vector3.Zero;
			intersection2 = Vector3.Zero;

			sphereCenter -= linePoint;

			var a = Vector3.Dot(lineVec, lineVec);
			var b = Vector3.Dot(lineVec, sphereCenter);
			var c = Vector3.Dot(sphereCenter, sphereCenter) - sphereRadius * sphereRadius;

			if (a == 0.0f) {
				// レイの長さがゼロ
				return false;
			}

			var s = b * b - a * c;
			if (s < 0.0f) {
				// 衝突していない
				return false;
			}

			s = (float)Math.Sqrt(s);
			var a1 = (b - s) / a;
			var a2 = (b + s) / a;

			if (a1 < 0.0f || a2 < 0.0f) {
				// レイの反対側で衝突
				return false;
			}
		
			intersection1 = linePoint + a1 * lineVec;
			intersection2 = linePoint + a2 * lineVec;

			return true;
		}

		#endregion

		#region utility

		/// <summary>
		/// 平行じゃない二つの直線においてもっとも近い点を求める
		/// 平行じゃなかったらtrueを返す
		/// </summary>
		public static bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
		{

			closestPointLine1 = Vector3.Zero;
			closestPointLine2 = Vector3.Zero;

			float a = Vector3.Dot(lineVec1, lineVec1);
			float b = Vector3.Dot(lineVec1, lineVec2);
			float e = Vector3.Dot(lineVec2, lineVec2);

			float d = a * e - b * b;

			//lines are not parallel
			if (d != 0.0f)
			{

				Vector3 r = linePoint1 - linePoint2;
				float c = Vector3.Dot(lineVec1, r);
				float f = Vector3.Dot(lineVec2, r);

				float s = (b * f - c * e) / d;
				float t = (a * f - c * b) / d;

				closestPointLine1 = linePoint1 + lineVec1 * s;
				closestPointLine2 = linePoint2 + lineVec2 * t;

				return true;
			}

			else
			{
				return false;
			}
		}

		/// <summary>
		/// point1 - point2 - point3のなす角が鋭角か
		/// </summary>
		public static bool IsSharpAngle(Vector3 point1, Vector3 point2, Vector3 point3)
		{
			// 内積が正だったら鋭角
			return Vector3.Dot(point1 - point2, point3 - point2) >= 0;
		}


		/// <summary>
		/// 点が線分上のどこにあるかを求める
		/// 点が線分を構成する直線状にない場合はうまく働かないのでProjectPointOnLineを使ってから呼ぶ
		/// </summary>
		/// <returns>0: 線分上にある / 1: 線分外のlinePoint1側にある / 2: 線分外のlinePoint2側にある</returns>
		public static int PointOnWhichSideOfSegment(Vector3 point, Vector3 segmentStartPoint, Vector3 segmentEndPoint)
		{

			var lineVec = segmentEndPoint - segmentStartPoint;
			var pointVec = point - segmentStartPoint;
			var dot = Vector3.Dot(pointVec, lineVec);

			//point is on side of linePoint2, compared to linePoint1
			if (dot > 0)
			{
				//point is on the line segment
				if (pointVec.Length() <= lineVec.Length())
				{
					return 0;
				}

				//point is not on the line segment and it is on the side of linePoint2
				else
				{
					return 2;
				}
			}

			//Point is not on side of linePoint2, compared to linePoint1.
			//Point is not on the line segment and it is on the side of linePoint1.
			else
			{
				return 1;
			}
		}

		/// <summary>
		/// 点と平面の最短距離を求める
		/// 点が平面の法線から見てどちら側にあるかで符号が付く
		/// </summary>
		public static float SignedDistancePlanePoint(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
		{

			return Vector3.Dot(planeNormal, (point - planePoint));
		}

		/// <summary>
		/// 二つのベクトルが平行か
		/// </summary>
		public static bool IsParallel(Vector3 vector1, Vector3 vector2)
		{
			return Vector3.Dot(vector1, vector2) == vector1.Length() * vector2.Length();
		}

		#endregion

	}

}
