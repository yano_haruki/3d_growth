﻿using SlimDX;
using System;


namespace WindowsFormsApp1
{
	public static class Extensions
	{
		public static Vector3 ToVector3(this Vector4 self)
		{
			return new Vector3(self.X, self.Y, self.Z);
		}

		public static System.Drawing.Bitmap GetBitmap(this FontType self)
		{
			switch (self) {
			case FontType.Gothic:
				return Properties.Resources.fnt_gothic_32bit;
			case FontType.Mincho:
				return Properties.Resources.fnt_mincho_32bit;
			default:
				throw new Exception("invalid FontType.");
			}
		}

		public static Vector3 Normalized(this Vector3 self)
		{
			return Vector3.Normalize(self);
		}

		public static Vector3 Multiply(this Vector3 a, Vector3 b)
		{
			return new Vector3(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
		}

		public static Vector3 Divide(this Vector3 a, Vector3 b)
		{
			return new Vector3(a.X / b.X, a.Y / b.Y, a.Z / b.Z);
		}

		public static float GetMaxElement(this Vector3 self)
		{
			return Math.Max(Math.Max(self.X, self.Y), self.Z);
		}

		/*
		public static Vector3 ToEuler(this Quaternion self)
		{
			var euler	= Vector3.Zero;
			var matrix	= Matrix.RotationQuaternion(self);

			if (Math.Abs(matrix.M23 - 1.0f) <= float.Epsilon) 
			{
				euler.X			= (float)Math.PI / 2.0f;
				euler.Y			= 0;
				euler.Z			= (float)Math.Atan2(matrix.M12, matrix.M11);
			}
			else if (Math.Abs(matrix.M23 + 1.0f) <= float.Epsilon)
			{
				euler.X			= (float)Math.PI / 2.0f * -1.0f;
				euler.Y			= 0;
				euler.Z			= (float)Math.Atan2(matrix.M12, matrix.M11);
			}
			else {
				euler.X			= (float)Math.Asin(matrix.M23);
				euler.Y			= (float)Math.Atan2(matrix.M13 * -1.0f, matrix.M33);
				euler.Z			= (float)Math.Atan2(matrix.M21 * -1.0f, matrix.M22);
			}
		
			return euler;
		}
		*/
	}
}