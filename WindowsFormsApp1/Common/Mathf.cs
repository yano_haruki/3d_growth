﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
	public static class Mathf
	{
		public static float Deg2Rad = (float)Math.PI / 180;
		public static float Rad2Deg = 180 / (float)Math.PI;
		public const float PI = (float)Math.PI;
		
		public static float Sin(float a) { return (float)Math.Sin(a); }
		public static float Cos(float a) { return (float)Math.Cos(a); }
		public static float Tan(float a) { return (float)Math.Tan(a); }
		public static float Asin(float d) { return (float)Math.Asin(d); }
		public static float Acos(float d) { return (float)Math.Acos(d); }
		public static float Atan2(float y, float x) { return (float)Math.Atan2(y, x); }

		public static float Clamp(float a, float min, float max)
		{
			a		= a < min ? min : a;
			a		= a > max ? max : a;
			return a;
		}
		public static int Clamp(int a, int min, int max)
		{
			a		= a < min ? min : a;
			a		= a > max ? max : a;
			return a;
		}
		public static float Clamp01(float a)
		{
			return Clamp(a, 0.0f, 1.0f);
		}
	}
}
