﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			using (var gameForm = new GameForm()) {
				gameForm.Run();
			}
		}
	}
}
