﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;

namespace WindowsFormsApp1
{
	public class Texture2D
	{
		public Size Size { get { return _source.Size; } }
		public int CubeMapSize { get { return _source.Size.Height / 3; } }

		private SlimDX.Direct3D11.Texture2D _texture;
		private Bitmap _source;
		
		public Texture2D(Bitmap bitmap, bool isCubeMap = false)
		{
			_source			= bitmap;
		}
		
		public SlimDX.Direct3D11.Texture2D GetTexture2D(SlimDX.Direct3D11.Device device)
		{
			if (_texture != null) {
				return _texture;
			}

			// テクスチャを生成
			using (MemoryStream stream = new MemoryStream()) 
			{
				// ImageFormat.Bmpにするとアルファつき画像が正常にが読み込まれない・・
				// Bmpが正しい気もするけど・・
				_source.Save(stream, ImageFormat.Png);
				stream.Position = 0;
				_texture		= SlimDX.Direct3D11.Texture2D.FromStream(device, stream, (int)stream.Length);
				return _texture;
			}
		}
	}
}
