﻿using SlimDX;
using SlimDX.Direct3D11;
using SlimDX.DXGI;
using Device = SlimDX.Direct3D11.Device;
using Resource = SlimDX.Direct3D11.Resource;

namespace WindowsFormsApp1
{
	public static class Graphics
	{
		public enum DebugModeType
		{
			Default,
			Wireframe,
		}

		public enum DrawTargetType
		{
			ShadowMap,
			RenderTarget,
			RenderTargetAndDepth
		}

		private const int RENDER_TARGET_COUNT	= 8;
		private static Device _device;
		private static SwapChain _swapChain;
		private static DeviceContext _context;
		private static RenderTargetView _renderTarget;
		private static Matrix _currentMatrixVP;
		private static Matrix _currentShadowMatrixVP;
		public static DebugModeType DebugMode { get; set; }
		private static DepthStencilView _depthStencil;
		private static ShadowMap _shadowMap;
		private static Viewport _viewport;

		public static void InitDevice(int width, int height, System.IntPtr outputHandle)
		{
			// スワップチェーンの情報を表す構造体を作成する
			var description = new SwapChainDescription()
			{
				// バッファの数。ダブルバッファリングするときには2を指定する
				BufferCount = 1,
				// true: ウィンドウモード / false: フルスクリーンモード
				IsWindowed = true,
				// レンダリング結果の横幅、縦幅、フォーマットなど
				ModeDescription = new ModeDescription(width, height, new Rational(60, 1), Format.R8G8B8A8_UNorm),
				// ウィンドウハンドル
				OutputHandle = outputHandle,
				// レンダリング結果を何に使うか
				Usage = Usage.RenderTargetOutput,
				// マルチサンプルの数とクオリティ
				SampleDescription = new SampleDescription(1, 0)
			};

			// スワップチェーンとともにGPUを初期化する
			Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.None, description, out _device, out _swapChain);


			//TOOD これ以下は別の場所にうつしたほうがよさそう？

			// レンダーターゲットを生成、初期化
			using (var backBuffer = Resource.FromSwapChain<SlimDX.Direct3D11.Texture2D>(_swapChain, 0))
			{
				_renderTarget = new RenderTargetView(_device, backBuffer);
			}

			// Depth Bufferを作成
			var depthBufferDesc = new Texture2DDescription
			{
				ArraySize = 1,
				BindFlags = BindFlags.DepthStencil,
				Format = Format.D32_Float,
				Width = ScreenUtility.Width,
				Height = ScreenUtility.Height,
				MipLevels = 1,
				SampleDescription = new SampleDescription(1, 0)
			};
			var depthBuffer = new SlimDX.Direct3D11.Texture2D(_device, depthBufferDesc);
			_depthStencil = new DepthStencilView(_device, depthBuffer);

			// ビューポートの設定
			_context		= _device.ImmediateContext;
			_viewport		= new Viewport(0.0f, 0.0f, width, height, 0, 1);

			// カリングなどを設定
			_context.Rasterizer.State		= RasterizerState.FromDescription
			(
				_device,
				new RasterizerStateDescription
				{
					CullMode = CullMode.Back,
					FillMode = FillMode.Solid
				}
			);

			// シャドウマップ初期化
			_shadowMap = new ShadowMap(_device, 2048, 2048);
		}

		public static void SetViewport(bool isShadow)
		{
			if (isShadow) {
				_shadowMap.SetViewports(_context);
			}
			else {
				_context.Rasterizer.SetViewports(_viewport);
			}
		}

		public static void SetDrawTargetType(DrawTargetType type)
		{
			switch (type) {
			case DrawTargetType.ShadowMap:
				_shadowMap.SetDrawTarget(_context);
				break;
			case DrawTargetType.RenderTarget:
				_context.OutputMerger.SetTargets(_renderTarget);
				break;
			case DrawTargetType.RenderTargetAndDepth:
				_context.OutputMerger.SetTargets(_depthStencil, _renderTarget);
				break;
			default:
				break;
			}
		}

		public static void SetCullMode(CullMode cullMode)
		{
			var currentState			= _context.Rasterizer.State;
			_context.Rasterizer.State	= RasterizerState.FromDescription
			(
				_device,
				new RasterizerStateDescription
				{
					CullMode			= cullMode,
					FillMode			= currentState.Description.FillMode,

				}
			);
		}

		public static void SetFillMode(FillMode fillMode)
		{
			var currentState			= _context.Rasterizer.State;
			_context.Rasterizer.State	= RasterizerState.FromDescription
			(
				_device,
				new RasterizerStateDescription
				{
					CullMode			= currentState.Description.CullMode,
					FillMode			= fillMode,
				}
			);
		}

		public static void SetBlend(BlendOption src, BlendOption dst, ColorWriteMaskFlags colormask = ColorWriteMaskFlags.All)
		{
			var blendDesc			= new BlendStateDescription() {
				AlphaToCoverageEnable = false,
				IndependentBlendEnable = false
			};
			for(int i = 0; i < RENDER_TARGET_COUNT; i++) {
				blendDesc.RenderTargets[i]							= new RenderTargetBlendDescription();
				blendDesc.RenderTargets[i].BlendEnable				= true;
				// UnityでいうBlendOp(SrcColorとDstColor間の計算方法。通常はAdd)
				blendDesc.RenderTargets[i].BlendOperation			= BlendOperation.Add;
				blendDesc.RenderTargets[i].BlendOperationAlpha		= BlendOperation.Add;
				// カラーマスク
				blendDesc.RenderTargets[i].RenderTargetWriteMask	= colormask;
				// RGBの合成方法
				blendDesc.RenderTargets[i].SourceBlend				= src;
				blendDesc.RenderTargets[i].DestinationBlend			= dst;
				// Alphaの合成方法
				blendDesc.RenderTargets[i].SourceBlendAlpha			= BlendOption.One;
				blendDesc.RenderTargets[i].DestinationBlendAlpha	= BlendOption.Zero;
			}
			_context.OutputMerger.BlendState					= BlendState.FromDescription(_device, blendDesc);

			// 下記多分不要？
			//_context.OutputMerger.BlendFactor = new Color4(1, 1, 1, 1);
			//_context.OutputMerger.BlendSampleMask = 0xffffff;
		}

		public static void Clear(Color4 color, bool isShadow)
		{
			// シャドウマップ用の分岐（とりあえずここ・・）
			if (isShadow) {
				_shadowMap.Clear(_context);
			}
			else {
				_context.ClearRenderTargetView(_renderTarget, color);
				_context.ClearDepthStencilView(_depthStencil, DepthStencilClearFlags.Depth, 1, 0);

			}
		}

		public static void SetMatrixVP(Matrix matrix)
		{
			_currentMatrixVP			= matrix;
		}

		public static void SetShadowMatrixVP(Matrix matrix)
		{
			_currentShadowMatrixVP = matrix;
		}

		/// <summary>
		/// メッシュを描画する
		/// </summary>
		public static void DrawMesh(Mesh mesh, Material material, Matrix matrixM)
		{
			material.SetMatrix("MATRIX_VP", _currentMatrixVP);
			material.SetMatrix("MATRIX_M", matrixM);

			material.PrepareDrawing(_device);

			material.GetEffectPath(_device, 0, 0).Apply(_context);
			_context.InputAssembler.InputLayout			= new InputLayout(_device, material.GetShaderSignature(), mesh.GetInputElements());
			_context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(mesh.GetVertexBuffer(_device), mesh.GetVertexSizeInBytes(), 0));
			_context.InputAssembler.SetIndexBuffer(mesh.GetIndexBuffer(_device), Format.R32_UInt, 0);
			_context.InputAssembler.PrimitiveTopology	= PrimitiveTopology.TriangleList;



			_context.DrawIndexed(mesh.IndexCount, 0, 0);
		}

		public static void DrawShadow(Mesh mesh, Material material, Matrix matrixM)
		{
			material.SetMatrix("MATRIX_VP", _currentShadowMatrixVP);
			material.SetMatrix("MATRIX_M", matrixM);

			material.PrepareDrawing(_device);

			material.GetEffectPath(_device, 0, 0).Apply(_context);
			_context.InputAssembler.InputLayout			= new InputLayout(_device, material.GetShaderSignature(), mesh.GetInputElements());
			_context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(mesh.GetVertexBuffer(_device), mesh.GetVertexSizeInBytes(), 0));
			_context.InputAssembler.SetIndexBuffer(mesh.GetIndexBuffer(_device), Format.R32_UInt, 0);
			_context.InputAssembler.PrimitiveTopology	= PrimitiveTopology.TriangleList;

			_context.DrawIndexed(mesh.IndexCount, 0, 0);

		}

		public static ShaderResourceView GetShadowMapResourceView()
		{
			// テスト用に適当なテクスチャを返してみる
			//return new ShaderResourceView(_device, new Texture2D(Properties.Resources.tex_green).GetTexture2D(_device));

			return _shadowMap.DepthMapSRV;
		}

		public static void PresentSwapChain()
		{
			_swapChain.Present(0, PresentFlags.None);
		}

		public static void DisposeDevice()
		{
			// 破棄
			_renderTarget.Dispose();
			_swapChain.Dispose();
			_context.Dispose();
			_device.Dispose();
		}
	}
}
