﻿using SlimDX;
using SlimDX.Direct3D11;
using System.Collections.Generic;
using Device = SlimDX.Direct3D11.Device;

namespace WindowsFormsApp1
{
	public class WeightInfo
	{
		public int BoneIndex { get; set; }
		public float Weight { get; set; }
	}

	public class BoneInfo
	{
		public int BoneIndex { get; set; }
		public string Name { get; set; }
		public Matrix InitPoseMatrix { get; set; }
		public Matrix InvInitPoseMatrix { get; set; }
	}

	public class SkinList
	{
		public BoneInfo[] BoneInfos { get; set; }
		// key : 頂点のインデックス value : weight
		public Dictionary<int, List<WeightInfo>> SkinWeights { get; set; }
	}

	// アニメーション用
	public class BonePoseList
	{
		// フレーム数、ボーン名、姿勢
		public Dictionary<int, Dictionary<string, Matrix>> Poses = new Dictionary<int, Dictionary<string, Matrix>>();
	}


	public class SkinnedMesh : Mesh
	{
		public SkinList[] SkinLists { get; set; }
		public BonePoseList PoseList { get; set; }
		public int FrameCount { get; set; }
		private Vector3[] _firstVertices;
		public Vector3[] FirstVertices { get { return _firstVertices; } set { _firstVertices = value; } }

		private VertexInfo[] _vertexInfo;

		public override Buffer GetVertexBuffer(Device device)
		{
			if (_vertexInfo == null) {
				var vertexInfo = new List<VertexInfo>();
				for (int i = 0; i < VertexCount; i++) {
					vertexInfo.Add
					(
						new VertexInfo
						{
							Position	= Vertices[i],
							Color		= Colors[i],
							TexCoord	= TexCoords[i],
							Normal		= Normals[i]
						}
					);
				}
				_vertexInfo = vertexInfo.ToArray();
			}

			if (FrameCount >= 0) {
				// モーション
				for (int vertexIndex = 0; vertexIndex < _vertexInfo.Length; vertexIndex++) {
					// 頂点に対応するウェイト情報を取得
					var weightInfo = SkinLists[0].SkinWeights[vertexIndex];
					var frameBoneInfo = PoseList.Poses[FrameCount];
					var position = Vector3.Zero;
					for (int i = 0; i < weightInfo.Count; i++) {
						var weight = weightInfo[i];
						var boneIndex = weightInfo[i].BoneIndex;
						var boneInfo = SkinLists[0].BoneInfos[boneIndex];
						var boneName = boneInfo.Name;

						// 座標変換
						var frameMatrix = frameBoneInfo[boneName];
						var matrix = boneInfo.InvInitPoseMatrix * frameMatrix;
						position += Vector3.Transform(Vertices[vertexIndex], matrix).ToVector3() * weight.Weight;
					}
					_vertexInfo[vertexIndex].Position = position;
				}

			}

			return CreateVertexBuffer(device);
		}

		private Buffer CreateVertexBuffer(Device device)
		{
			using (DataStream vertexStream  = new DataStream(_vertexInfo, true, true))
			{
				return new Buffer(
					device,
					vertexStream,
					new BufferDescription
					{
						SizeInBytes= (int)vertexStream.Length,
						BindFlags = BindFlags.VertexBuffer,
					}
				);
			}
		}

	}
}
