﻿using SlimDX;
using SlimDX.Direct3D11;
using SlimDX.DXGI;
using System;
using System.Collections.Generic;

namespace WindowsFormsApp1
{
	[Flags]
	public enum VertexType
	{
		Position = 1 << 0,
		Color = 1 << 1,
		TexCoord = 1 << 2,
		Normal = 1 << 3,
		Tangent = 1 << 4
	}

	public struct VertexInfo
	{
		public Vector3 Position;
		public Color4 Color;
		public Vector2 TexCoord;
		public Vector3 Normal;
		public Vector4 Tangent;

		/*
		public static readonly InputElement[] VertexElements = new[]
		{
			new InputElement
			{
				SemanticName		= "SV_Position",
				Format				= Format.R32G32B32_Float
			},
			new InputElement
			{
				SemanticName		= "COLOR",
				Format				= Format.R32G32B32A32_Float,
				AlignedByteOffset	= InputElement.AppendAligned
			},
			new InputElement
			{
				SemanticName		= "TEXCOORD",
				Format				= Format.R32G32_Float,
				AlignedByteOffset	= InputElement.AppendAligned
			},
			new InputElement
			{
				SemanticName		= "NORMAL",
				Format				= Format.R32G32B32_Float,
				AlignedByteOffset	= InputElement.AppendAligned
			}
		};
		*/

		public static InputElement[] GetVertexElement(VertexType types)
		{
			var elements = new List<InputElement>();
			if ((types & VertexType.Position) == VertexType.Position) {
				elements.Add(new InputElement
				{
					SemanticName = "SV_Position",
					Format = Format.R32G32B32_Float
				});
			}
			if ((types & VertexType.Color) == VertexType.Color) {
				elements.Add(new InputElement
				{
					SemanticName = "COLOR",
					Format = Format.R32G32B32A32_Float,
					AlignedByteOffset = InputElement.AppendAligned
				});
			}
			if ((types & VertexType.TexCoord) == VertexType.TexCoord) {
				elements.Add(new InputElement
				{
					SemanticName = "TEXCOORD",
					Format = Format.R32G32_Float,
					AlignedByteOffset = InputElement.AppendAligned
				});
			}
			if ((types & VertexType.Normal) == VertexType.Normal) {
				elements.Add(new InputElement
				{
					SemanticName = "NORMAL",
					Format = Format.R32G32B32_Float,
					AlignedByteOffset = InputElement.AppendAligned
				});
			}
			if ((types & VertexType.Tangent) == VertexType.Tangent) {
				elements.Add(new InputElement
				{
					SemanticName = "TANGENT",
					Format = Format.R32G32B32A32_Float,
					AlignedByteOffset = InputElement.AppendAligned
				});
			}

			return elements.ToArray();
		}

		public static int GetSizeInBytes(VertexType types)
		{
			return System.Runtime.InteropServices.Marshal.SizeOf(typeof(VertexInfo));

			/*
			var size = 0;
			if ((types & VertexType.Position) == VertexType.Position) {
				size += System.Runtime.InteropServices.Marshal.SizeOf(typeof(Vector3));
			}
			if ((types & VertexType.Color) == VertexType.Color) {
				size += System.Runtime.InteropServices.Marshal.SizeOf(typeof(Color4));
			}
			if ((types & VertexType.TexCoord) == VertexType.TexCoord) {
				size += System.Runtime.InteropServices.Marshal.SizeOf(typeof(Vector2));
			}
			if ((types & VertexType.Normal) == VertexType.Normal) {
				size += System.Runtime.InteropServices.Marshal.SizeOf(typeof(Vector3));
			}

			return size;
			*/
		}
	}
}
