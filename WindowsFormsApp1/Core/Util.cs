﻿using System;

namespace WindowsFormsApp1
{
	public static class Util {

        public static void ReleaseCom<T>(ref T x) where T : class, IDisposable {
            if (x != null) {
                x.Dispose();
                x = null;
            }
        }
    }
}
