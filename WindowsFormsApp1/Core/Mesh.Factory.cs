﻿using SlimDX;
using System;

namespace WindowsFormsApp1
{
	public partial class Mesh
	{
		/*
		/// <summary>
		/// 矩形を作る
		/// </summary>
		public static Mesh CreateRectangle()
		{
			var mesh		= new Mesh();
			mesh.Vertices			= new List<Vector3>()
			{
				new Vector3(-1.0f, -1.0f, 0.0f),
				new Vector3(1.0f, -1.0f, 0.0f),
				new Vector3(-1.0f, 1.0f, 0.0f),
				new Vector3(1.0f, 1.0f, 0.0f)

			}
			.ToArray();
			mesh.Colors			= new List<Color4>()
			{
				new Color4(1.0f, 1.0f, 0.0f, 0.0f),
				new Color4(1.0f, 0.0f, 1.0f, 0.0f),
				new Color4(1.0f, 0.0f, 0.0f, 1.0f),
				new Color4(1.0f, 0.0f, 0.0f, 1.0f)
			}
			.ToArray();
			mesh.TexCoords		= new List<Vector2>()
			{
				new Vector2(0.0f, 1.0f),
				new Vector2(1.0f, 1.0f),
				new Vector2(0.0f, 0.0f),
				new Vector2(1.0f, 0.0f)
			}
			.ToArray();
			mesh.Indices		= new List<int>()
			{
				0, 2, 1, 1, 2, 3
			}
			.ToArray();

			return mesh;
		}

		public static Mesh CreateTriangle()
		{
			var mesh		= new Mesh();
			mesh.Vertices			= new List<Vector3>()
			{
				new Vector3(-1.0f, 0.0f, 0.0f),
				new Vector3(1.0f, 0.0f, 0.0f),
				new Vector3(0.0f, 1.0f, 0.0f)

			}
			.ToArray();
			mesh.Colors			= new List<Color4>()
			{
				new Color4(1.0f, 1.0f, 0.0f, 0.0f),
				new Color4(1.0f, 0.0f, 1.0f, 0.0f),
				new Color4(1.0f, 0.0f, 0.0f, 1.0f)
			}
			.ToArray();
			mesh.TexCoords		= new List<Vector2>()
			{
				new Vector2(0.0f, 1.0f),
				new Vector2(1.0f, 1.0f),
				new Vector2(0.5f, 0.0f)
			}
			.ToArray();
			mesh.Indices		= new List<int>()
			{
				0,
				1,
				2
			}
			.ToArray();

			return mesh;
		}
		*/

		#region Plane

		public static Mesh CreatePlane(float length = 1, float width = 1, int resX = 2, int resZ = 2)
		{
			if (resX < 2 || resZ < 2) {
				throw new Exception("resX / resZ must be more than 2.");
			}

			var mesh		= new Mesh();

			// 頂点
			var vertices		= new Vector3[resX * resZ];
			for(int z = 0; z < resZ; z++)
			{
				// [ -length / 2, length / 2 ]
				float zPos = ((float)z / (resZ - 1) - .5f) * length;
				for(int x = 0; x < resX; x++)
				{
					// [ -width / 2, width / 2 ]
					float xPos = ((float)x / (resX - 1) - .5f) * width;
					vertices[ x + z * resX ] = new Vector3( xPos, 0f, zPos );
				}
			}
			mesh.Vertices		= vertices;

			// Color
			var colors			= new Color4[vertices.Length];
			for (int i = 0; i < colors.Length; i++) {
				colors[i]		= new Color4(1.0f, 1.0f, 1.0f, 1.0f);
			}
			mesh.Colors			= colors;

			// UV
			var uvs = new Vector2[vertices.Length];
			for(int v = 0; v < resZ; v++)
			{
				for(int u = 0; u < resX; u++)
				{
					uvs[ u + v * resX ] = new Vector2( (float)u / (resX - 1), (float)v / (resZ - 1) );
				}
			}
			mesh.TexCoords		= uvs;


			int nbFaces			= (resX - 1) * (resZ - 1);
			var indices			= new int[nbFaces * 6];
			int t = 0;
			for(int face = 0; face < nbFaces; face++ )
			{
				// Retrieve lower left corner from face ind
				int i = face % (resX - 1) + (face / (resZ - 1) * resX);

				indices[t++]	= i + resX;
				indices[t++]	= i + 1;
				indices[t++]	= i;

				indices[t++]	= i + resX;
				indices[t++]	= i + resX + 1;
				indices[t++]	= i + 1;
			}
			mesh.Indices		= indices;



			// 法線
			var normales		= new Vector3[ vertices.Length ];
			for( int n = 0; n < normales.Length; n++ )
			{
				normales[n] = Vector3.UnitY;
			}
			mesh.Normals		= normales;

			mesh.VertexTypes = VertexType.Position | VertexType.TexCoord | VertexType.Color | VertexType.Normal;

			return mesh;
		}

		#endregion

		#region Cube

		public static Mesh CreateCube(float length = 1.0f, float width = 1.0f, float height = 1.0f)
		{
			var mesh		= new Mesh();

			Vector3 p0 = new Vector3( -length * .5f,	-width * .5f, height * .5f );
			Vector3 p1 = new Vector3( length * .5f, 	-width * .5f, height * .5f );
			Vector3 p2 = new Vector3( length * .5f, 	-width * .5f, -height * .5f );
			Vector3 p3 = new Vector3( -length * .5f,	-width * .5f, -height * .5f );

			Vector3 p4 = new Vector3( -length * .5f,	width * .5f,  height * .5f );
			Vector3 p5 = new Vector3( length * .5f, 	width * .5f,  height * .5f );
			Vector3 p6 = new Vector3( length * .5f, 	width * .5f,  -height * .5f );
			Vector3 p7 = new Vector3( -length * .5f,	width * .5f,  -height * .5f );

			Vector3[] vertices = new Vector3[]
			{
				// Bottom
				p0, p1, p2, p3,

				// Left
				p7, p4, p0, p3,

				// Front
				p4, p5, p1, p0,

				// Back
				p6, p7, p3, p2,

				// Right
				p5, p6, p2, p1,

				// Top
				p7, p6, p5, p4
			};
			mesh.Vertices		= vertices;

			// Color
			var colors			= new Color4[vertices.Length];
			for (int i = 0; i < colors.Length; i++) {
				colors[i]		= new Color4(1.0f, 1.0f, 1.0f, 1.0f);
			}
			mesh.Colors			= colors;

			// UV
			/*
			Vector2 _00 = new Vector2( 0f, 0f );
			Vector2 _10 = new Vector2( 1f, 0f );
			Vector2 _01 = new Vector2( 0f, 1f );
			Vector2 _11 = new Vector2( 1f, 1f );
			*/
			// Unity用コード（上記）をDirectX用に変える
			Vector2 _00 = new Vector2( 0f, 1f );
			Vector2 _10 = new Vector2( 1f, 1f );
			Vector2 _01 = new Vector2( 0f, 0f );
			Vector2 _11 = new Vector2( 1f, 0f );

			Vector2[] uvs = new Vector2[]
			{
				// Bottom
				_11, _01, _00, _10,

				// Left
				_11, _01, _00, _10,

				// Front
				_11, _01, _00, _10,

				// Back
				_11, _01, _00, _10,

				// Right
				_11, _01, _00, _10,

				// Top
				_11, _01, _00, _10,
			};
			mesh.TexCoords			= uvs;

			int[] triangles = new int[]
			{
				// Bottom
				3, 1, 0,
				3, 2, 1,

				// Left
				3 + 4 * 1, 1 + 4 * 1, 0 + 4 * 1,
				3 + 4 * 1, 2 + 4 * 1, 1 + 4 * 1,

				// Front
				3 + 4 * 2, 1 + 4 * 2, 0 + 4 * 2,
				3 + 4 * 2, 2 + 4 * 2, 1 + 4 * 2,

				// Back
				3 + 4 * 3, 1 + 4 * 3, 0 + 4 * 3,
				3 + 4 * 3, 2 + 4 * 3, 1 + 4 * 3,

				// Right
				3 + 4 * 4, 1 + 4 * 4, 0 + 4 * 4,
				3 + 4 * 4, 2 + 4 * 4, 1 + 4 * 4,

				// Top
				3 + 4 * 5, 1 + 4 * 5, 0 + 4 * 5,
				3 + 4 * 5, 2 + 4 * 5, 1 + 4 * 5,

			};
			mesh.Indices			= triangles;

			// 法線
			Vector3 up 	= Vector3.UnitY;
			Vector3 down 	= Vector3.UnitY * -1;
			Vector3 front 	= Vector3.UnitZ;
			Vector3 back 	= Vector3.UnitZ * -1;
			Vector3 left 	= Vector3.UnitX * -1;
			Vector3 right 	= Vector3.UnitX;

			Vector3[] normales = new Vector3[]
			{
				// Bottom
				down, down, down, down,

				// Left
				left, left, left, left,

				// Front
				front, front, front, front,

				// Back
				back, back, back, back,

				// Right
				right, right, right, right,

				// Top
				up, up, up, up
			};
			mesh.Normals	= normales;

			mesh.VertexTypes = VertexType.Position | VertexType.TexCoord | VertexType.Color | VertexType.Normal;

			return mesh;
		}

		#endregion

		#region Sphere

		public static Mesh CreateSphere(float radius = 0.5f, int nbLong = 20, int nbLat = 20)
		{
			var mesh		= new Mesh();

			// 頂点
			Vector3[] vertices = new Vector3[(nbLong+1) * nbLat + 2];
			float _pi = Mathf.PI;
			float _2pi = _pi * 2f;
			vertices[0] = Vector3.UnitY * radius;
			for( int lat = 0; lat < nbLat; lat++ )
			{
				float a1 = _pi * (float)(lat+1) / (nbLat+1);
				float sin1 = Mathf.Sin(a1);
				float cos1 = Mathf.Cos(a1);

				for( int lon = 0; lon <= nbLong; lon++ )
				{
					float a2 = _2pi * (float)(lon == nbLong ? 0 : lon) / nbLong;
					float sin2 = Mathf.Sin(a2);
					float cos2 = Mathf.Cos(a2);

					vertices[ lon + lat * (nbLong + 1) + 1] = new Vector3( sin1 * cos2, cos1, sin1 * sin2 ) * radius;
				}
			}
			vertices[vertices.Length-1] = Vector3.UnitY * -radius;
			mesh.Vertices		= vertices;

			// Color
			var colors			= new Color4[vertices.Length];
			for (int j = 0; j < colors.Length; j++) {
				colors[j]		= new Color4(1.0f, 1.0f, 1.0f, 1.0f);
			}
			mesh.Colors			= colors;


			// UV
			Vector2[] uvs = new Vector2[vertices.Length];
			uvs[0] = Vector2.UnitY;
			uvs[uvs.Length-1] = Vector2.Zero;
			for( int lat = 0; lat < nbLat; lat++ )
				for( int lon = 0; lon <= nbLong; lon++ )
					uvs[lon + lat * (nbLong + 1) + 1] = new Vector2( (float)lon / nbLong, /*1.0f -*/ (float)(lat+1) / (nbLat+1) ); // Unity用コードをDirectX用に変えるため 1.0f - をコメントアウト
			mesh.TexCoords			= uvs;

			// Indices
			int nbFaces = vertices.Length;
			int nbTriangles = nbFaces * 2;
			int nbIndexes = nbTriangles * 3;
			int[] triangles = new int[ nbIndexes ];
			//Top Cap
			int i = 0;
			for( int lon = 0; lon < nbLong; lon++ )
			{
				triangles[i++] = lon+2;
				triangles[i++] = lon+1;
				triangles[i++] = 0;
			}
			//Middle
			for( int lat = 0; lat < nbLat - 1; lat++ )
			{
				for( int lon = 0; lon < nbLong; lon++ )
				{
					int current = lon + lat * (nbLong + 1) + 1;
					int next = current + nbLong + 1;

					triangles[i++] = current;
					triangles[i++] = current + 1;
					triangles[i++] = next + 1;

					triangles[i++] = current;
					triangles[i++] = next + 1;
					triangles[i++] = next;
				}
			}
			//Bottom Cap
			for( int lon = 0; lon < nbLong; lon++ )
			{
				triangles[i++] = vertices.Length - 1;
				triangles[i++] = vertices.Length - (lon+2) - 1;
				triangles[i++] = vertices.Length - (lon+1) - 1;
			}
			mesh.Indices			= triangles;

			// 法線
			Vector3[] normales = new Vector3[vertices.Length];
			for( int n = 0; n < vertices.Length; n++ )
				normales[n] = vertices[n].Normalized();
			mesh.Normals	= normales;

			mesh.VertexTypes = VertexType.Position | VertexType.TexCoord | VertexType.Color | VertexType.Normal;

			return mesh;

		}

		#endregion

		#region Capsule

		/// <summary>
		/// position of the model pivot
		/// </summary>
		public enum PivotPosition
		{
			/// <summary>
			/// pivot is at bottom
			/// </summary>
			Botttom,

			/// <summary>
			/// pivot is in center
			/// </summary>
			Center,

			/// <summary>
			/// pivot is on top
			/// </summary>
			Top,
		}


		public static Mesh CreateCapsule(float radius = 0.5f, float height = 1.0f, int sides = 20, int heightSegments = 20, bool preserveHeight = false, PivotPosition pivotPosition = PivotPosition.Center)
		{
			var mesh		= new Mesh();

			radius = Mathf.Clamp(radius, 0, 100);
			height = Mathf.Clamp(height, 0, 100);
			heightSegments = Mathf.Clamp(heightSegments, 1, 250);
			sides = Mathf.Clamp(sides, 4, 250);

			if (preserveHeight)
			{
				height = height - radius * 2;
				if (height < 0)
				{
					height = 0;
				}
			}

			int rings = sides;
			int sectors = sides+1;

			if ((rings&1) == 0)
			{
				rings += 1;
				sectors = sides+1;
			}

			float R = 1 / (float)(rings - 1);
			float S = 1 / (float)(sectors - 1);
			var midRing = rings/2 + 1;

			int verticesNum = 0;
			var trianglesNum = (rings - 1) * (sectors - 1) * 6;
			var verticesCylinder = (sides + 1) * (heightSegments + 1);
			var trianglesCylinder = sides * 6 * heightSegments;

			verticesNum = rings*sectors +sectors;


			var vertices = new Vector3[verticesNum + verticesCylinder];
			var normals = new Vector3[verticesNum + verticesCylinder];
			var uvs = new Vector2[verticesNum + verticesCylinder];
			var triangles = new int[trianglesNum + trianglesCylinder];

			var capsuleRadius = radius + height/2;

			var pivotOffset = Vector3.Zero;
			switch (pivotPosition)
			{
				case PivotPosition.Botttom: pivotOffset = new Vector3(0.0f, capsuleRadius, 0.0f);
					break;
				case PivotPosition.Top: pivotOffset = new Vector3(0.0f, -capsuleRadius, 0.0f);
					break;
			}

			var vertIndex = 0;
			var triIndex = 0;

			var vertIndexCyl = 0;
			var triIndexCyl = 0;

			// generate upper hemisphere
			for (int r = 0; r < midRing; r++)
			{
				var y = Mathf.Cos(-Mathf.PI * 2.0f + Mathf.PI * r * R);
				var sinR = Mathf.Sin(Mathf.PI * r * R);

				for (int s = 0; s < sectors; s++)
				{
					float x = Mathf.Sin(2*Mathf.PI*s*S)*sinR;
					float z = Mathf.Cos(2 * Mathf.PI * s * S) * sinR;

					vertices[vertIndex + 0] = new Vector3(x, y, z) * radius + pivotOffset;
					normals[vertIndex + 0] = new Vector3(x, y, z);

					vertices[vertIndex + 0].Y += height / 2;

					var uv = GetSphericalUV(vertices[vertIndex + 0] - pivotOffset);
					//uvs[vertIndex + 0] = new Vector2(s * S, 1.0f - (r * R));
					uvs[vertIndex + 0] = new Vector2(1.0f - s * S, uv.Y);


					if (r < midRing-1 && s < sectors - 1)
					{
						triangles[triIndex + 0] = (r + 1) * sectors + (s);
						triangles[triIndex + 1] = r * sectors + (s + 1);
						triangles[triIndex + 2] = r * sectors + s;

						triangles[triIndex + 3] = (r + 1) * sectors + (s + 1);
						triangles[triIndex + 4] = r * sectors + (s + 1);
						triangles[triIndex + 5] = (r + 1) * sectors + (s);

						triIndex += 6;
					}

					vertIndex += 1;
				}
			}

			// generate central cylinder
			if (height > 0)
			{
				vertIndexCyl = verticesNum;
				triIndexCyl = trianglesNum;
				var triVertCyl = verticesNum;
				var heightRatio = height/heightSegments;
				var bottomCenter = new Vector3(0.0f, -height/2, 0.0f);

				var sinR = Mathf.Sin(Mathf.PI * (midRing-1) * R);

				for (int s = 0; s <= sides; s++)
				{
					float x = Mathf.Sin(2 * Mathf.PI * s * S) * sinR;
					float z = Mathf.Cos(2 * Mathf.PI * s * S) * sinR;

					var v0 = new Vector3(x, 0.0f, z);
//                        var texV = (midRing - 1) * R;

					var currHeight = 0.0f;

					for (int j = 0; j <= heightSegments; j++)
					{
						// generate vertices
						vertices[vertIndexCyl + 0] = bottomCenter + v0 * radius + new Vector3(0.0f, currHeight, 0.0f) + pivotOffset;
						normals[vertIndexCyl + 0] = v0;
//                            uvs[vertIndexCyl + 0] = new Vector2(s * S, texV/2);
//
//                            texV += 1.0f/heightSegments;

						var uv = GetSphericalUV(vertices[vertIndexCyl + 0] - pivotOffset);
//                        uvs[vertIndex + 0] = new Vector2(s * S, 1.0f - (r * R));
						uvs[vertIndexCyl + 0] = new Vector2(1.0f - s * S, uv.Y);

						vertIndexCyl += 1;
						currHeight += heightRatio;
					}
				}

				for (int i = 0; i < sides; i++)
				{
					var triVertNext = verticesNum + (i + 1)*(heightSegments + 1);

					for (int j = 0; j < heightSegments; j++)
					{
						triangles[triIndexCyl + 0] = triVertNext + 0;
						triangles[triIndexCyl + 1] = triVertNext + 1;
						triangles[triIndexCyl + 2] = triVertCyl + 0;

						triangles[triIndexCyl + 3] = triVertNext + 1;
						triangles[triIndexCyl + 4] = triVertCyl + 1;
						triangles[triIndexCyl + 5] = triVertCyl + 0;

						triIndexCyl += 6;
						triVertCyl += 1;
						triVertNext += 1;
					}

					triVertCyl += 1;
				}
			}

			// generate bottom hemisphere
			for (int r = midRing-1; r < rings; r++)
			{
				var y = Mathf.Cos(-Mathf.PI * 2.0f + Mathf.PI * r * R);
				var sinR = Mathf.Sin(Mathf.PI * r * R);

				for (int s = 0; s < sectors; s++)
				{
					float x = Mathf.Sin(2 * Mathf.PI * s * S) * sinR;
					float z = Mathf.Cos(2 * Mathf.PI * s * S) * sinR;

					vertices[vertIndex + 0] = new Vector3(x, y, z) * radius;
					normals[vertIndex + 0] = new Vector3(x, y, z);

					vertices[vertIndex] += pivotOffset;

					vertices[vertIndex + 0].Y -= height/2;

					var uv = GetSphericalUV(vertices[vertIndex + 0] - pivotOffset);
//                        uvs[vertIndex + 0] = new Vector2(s * S, 1.0f - (r * R));
					uvs[vertIndex + 0] = new Vector2(1.0f - s * S, uv.Y);

					if (r < rings-1 && s < sectors - 1)
					{
						triangles[triIndex + 0] = ((r+1) + 1) * sectors + (s);
						triangles[triIndex + 1] = (r+1) * sectors + (s + 1);
						triangles[triIndex + 2] = (r+1) * sectors + s;

						triangles[triIndex + 3] = ((r+1) + 1) * sectors + (s + 1);
						triangles[triIndex + 4] = (r+1) * sectors + (s + 1);
						triangles[triIndex + 5] = ((r+1) + 1) * sectors + (s);

						triIndex += 6;
					}

					vertIndex += 1;
				}
			}

			// Color
			var colors			= new Color4[vertices.Length];
			for (int j = 0; j < colors.Length; j++) {
				colors[j]		= new Color4(1.0f, 1.0f, 1.0f, 1.0f);
			}
			mesh.Colors			= colors;

			mesh.Vertices = vertices;
			mesh.Normals = normals;
			mesh.TexCoords = uvs;
			mesh.Indices = triangles;

			mesh.VertexTypes = VertexType.Position | VertexType.TexCoord | VertexType.Color | VertexType.Normal;

			return mesh;
		}

        static Vector2 GetSphericalUV(Vector3 pnt)
        {
            var v0 = pnt.Normalized();

            return new Vector2((0.5f + Mathf.Atan2(v0.Z, v0.X) / (Mathf.PI * 2.0f)),
                                /*1.0f - */(0.5f - Mathf.Asin(v0.Y) / Mathf.PI)); // UnityとDirectXの互換性のため 1.0f - を削除
        }

		#endregion
	}
}
