﻿using SlimDX;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace WindowsFormsApp1
{
	/*
		public static class FBXManagerExtensions
		{
			public static Mesh Convert(this FBXManager.Mesh self)
			{
				var mesh = new Mesh();
				mesh.Vertices = self.Positions;
				mesh.Indices = self.indices;

				mesh.Colors = self.Colors;

				return mesh;
			}
		}
		*/
	/// <summary>
	/// FBXManager
	/// </summary>
	public class FBXManager
	{
		#region define

		/// C++のメッシュ構造体
		[StructLayout(LayoutKind.Sequential)]
		public struct CPP_Mesh
		{
			public IntPtr pName;
			public int VertexCount;
			public int TriangleCount;
			public int SkinListCount;
			public IntPtr pPositions;
			public IntPtr pColors;
			public IntPtr pNormals;
			public IntPtr pTangents;
			public IntPtr pUv0;
			public IntPtr pUv1;
			public IntPtr pIndices;
			public IntPtr pSkinLists;
		}

		/// メッシュリストの構造体
		[StructLayout(LayoutKind.Sequential)]
		public struct CPP_MeshList
		{
			public int Count;
			public IntPtr pMeshes;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct CPP_Skin
		{
			public int Count;
			public IntPtr pName;
			public IntPtr pIndices;
			public IntPtr pWeights;
			public IntPtr pInitMatrix;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct CPP_SkinList
		{
			public int Count;
			public IntPtr pSkins;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct CPP_BonePose
		{
			public int frameCount;
			public IntPtr pName;
			public IntPtr pMatrix;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct CPP_BonePoseList
		{
			public int Count;
			public IntPtr pPoses;
		}




		/*
		public class Mesh
		{
			public string Name;
			public int VertexCount;
			public int TriangleCount;
			public Vector3[] Positions;
			public Color4[] Colors;
			public Vector2[] uv0;
			public Vector2[] uv1;
			public int[] indices;
		}
		*/

		#endregion

		#region variable

		/// シングルトンインスタンス
		private static FBXManager _instance;
		public static FBXManager Instance {
			get {
				if (_instance == null) {
					_instance = new FBXManager();
				}
				return _instance;
			}
		}

		#endregion

		#region method

		/// 静的コンストラクタ
		static FBXManager() {
			_instance = new FBXManager();
		}

		[DllImport("3DGrowthFbxDll32.dll", EntryPoint = "CreateMeshList", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern bool DllCreateMeshList32(ref CPP_MeshList meshList, string pFileName);
		[DllImport("3DGrowthFbxDll64.dll", EntryPoint = "CreateMeshList", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern bool DllCreateMeshList64(ref CPP_MeshList meshList, string pFileName);
		public static bool DllCreateMeshList(ref CPP_MeshList meshList, string pFileName)
		{
			if (Environment.Is64BitProcess) {
				return DllCreateMeshList64(ref meshList, pFileName);
			}
			else {
				return DllCreateMeshList32(ref meshList, pFileName);
			}
		}

		[DllImport("3DGrowthFbxDll32.dll", EntryPoint = "DestroyMeshList", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern bool DllDestroyMeshList32(ref CPP_MeshList meshList);
		[DllImport("3DGrowthFbxDll64.dll", EntryPoint = "DestroyMeshList", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern bool DllDestroyMeshList64(ref CPP_MeshList meshList);
		public static bool DllDestroyMeshList(ref CPP_MeshList meshList)
		{
			if (Environment.Is64BitProcess) {
				return DllDestroyMeshList64(ref meshList);
			}
			else {
				return DllDestroyMeshList32(ref meshList);
			}
		}

		[DllImport("3DGrowthFbxDll32.dll", EntryPoint = "CreateFrameInfo", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern bool DllCreateFrameInfo32(ref CPP_BonePoseList meshList, string pFileName, int startFrame, int frameCount);
		[DllImport("3DGrowthFbxDll64.dll", EntryPoint = "CreateFrameInfo", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern bool DllCreateFrameInfo64(ref CPP_BonePoseList meshList, string pFileName, int startFrame, int frameCount);
		public static bool DllCreateFrameInfo(ref CPP_BonePoseList bonePoseList, string pFileName, int startFrame, int frameCount)
		{
			if (Environment.Is64BitProcess) {
				return DllCreateFrameInfo64(ref bonePoseList, pFileName, startFrame, frameCount);
			}
			else {
				return DllCreateFrameInfo32(ref bonePoseList, pFileName, startFrame, frameCount);
			}
		}

		[DllImport("3DGrowthFbxDll32.dll", EntryPoint = "DestroyFrameInfo", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern bool DllDestroyFrameInfo32(ref CPP_BonePoseList meshList);
		[DllImport("3DGrowthFbxDll64.dll", EntryPoint = "DestroyFrameInfo", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
		public static extern bool DllDestroyFrameInfot64(ref CPP_BonePoseList meshList);
		public static bool DllDestroyFrameInfo(ref CPP_BonePoseList meshList)
		{
			if (Environment.Is64BitProcess) {
				return DllDestroyFrameInfot64(ref meshList);
			}
			else {
				return DllDestroyFrameInfo32(ref meshList);
			}
		}


		/// <summary>
		/// メッシュを生成する
		/// </summary>
		public Mesh[] CreateMeshList(string filePath)
		{
			// CppMeshリスト情報の生成
			var cppMeshList = new CPP_MeshList();
			if (!DllCreateMeshList(ref cppMeshList, filePath)) {
				Console.WriteLine("Create Mesh Is Failed");
				return null;
			}
			Console.WriteLine("Create Mesh Is Succes");

			// Mesh配列の生成
			var meshes = new Mesh[cppMeshList.Count];
			int size = Marshal.SizeOf(typeof(CPP_Mesh));
			for (int i = 0; i < meshes.Length; i++) {
				IntPtr adderss = cppMeshList.pMeshes + size * i;
				var cppMesh = Marshal.PtrToStructure<CPP_Mesh>(adderss);

				var mesh = new Mesh();
				//mesh.VertexCount = cppMesh.VertexCount;
				//mesh.TriangleCount = cppMesh.TriangleCount;
				//mesh.Name = Marshal.PtrToStringAnsi(cppMesh.pName);
				mesh.Indices = new int[cppMesh.VertexCount];
				Marshal.Copy(cppMesh.pIndices, mesh.Indices, 0, cppMesh.VertexCount);

				var positions = new float[cppMesh.VertexCount * 3];
				mesh.Vertices = new Vector3[cppMesh.VertexCount];
				//mesh.FirstVertices = new Vector3[cppMesh.VertexCount];

				Marshal.Copy(cppMesh.pPositions, positions, 0, cppMesh.VertexCount * 3);
				for (int j = 0; j < cppMesh.VertexCount; j++) {
					var index = j * 3;
					mesh.Vertices[j] = new Vector3(positions[index], positions[index + 1], positions[index + 2]);
					//mesh.FirstVertices[j] = new Vector3(positions[index], positions[index + 1], positions[index + 2]);
				}

				if (cppMesh.pColors != IntPtr.Zero) {
					var colors = new float[cppMesh.VertexCount * 4];
					mesh.Colors = new Color4[cppMesh.VertexCount];
					Marshal.Copy(cppMesh.pColors, colors, 0, cppMesh.VertexCount * 4);
					for (int j = 0; j < cppMesh.VertexCount; j++) {
						var index = j * 4;
						mesh.Colors[j] = new Color4(colors[index + 3], colors[index], colors[index + 1], colors[index + 2]);
					}
				}

				// 法線
				if (cppMesh.pNormals != IntPtr.Zero) {
					var normals = new float[cppMesh.VertexCount * 3];
					mesh.Normals = new Vector3[cppMesh.VertexCount];
					Marshal.Copy(cppMesh.pNormals, normals, 0, cppMesh.VertexCount * 3);
					for (int j = 0; j < cppMesh.VertexCount; j++) {
						var index = j * 3;
						mesh.Normals[j] = new Vector3(normals[index], normals[index + 1], normals[index + 2]);
					}
				}

				// 従法線
				if (cppMesh.pTangents != IntPtr.Zero) {
					var tangents = new float[cppMesh.VertexCount * 4];
					mesh.Tangents = new Vector4[cppMesh.VertexCount];
					Marshal.Copy(cppMesh.pTangents, tangents, 0, cppMesh.VertexCount * 4);
					for (int j = 0; j < cppMesh.VertexCount; j++) {
						var index = j * 4;
						mesh.Tangents[j] = new Vector4(tangents[index], tangents[index + 1], tangents[index + 2], tangents[index + 3]);
					}
				}

				if (cppMesh.pUv0 != IntPtr.Zero) {
					var uv0 = new float[cppMesh.VertexCount * 2];
					mesh.TexCoords = new Vector2[cppMesh.VertexCount];
					Marshal.Copy(cppMesh.pUv0, uv0, 0, cppMesh.VertexCount * 2);
					for (int j = 0; j < cppMesh.VertexCount; j++) {
						var index = j * 2;
						mesh.TexCoords[j] = new Vector2(uv0[index], uv0[index + 1]);
					}
				}

				mesh.VertexTypes = VertexType.Position | VertexType.TexCoord | VertexType.Color | VertexType.Normal | VertexType.Tangent;

				// スキン情報
				/*
				if (cppMesh.SkinListCount != 0) {
					var skinList = new SkinList[cppMesh.SkinListCount];
					int skinListSize = Marshal.SizeOf(typeof(CPP_SkinList));
					for (int j = 0; j < cppMesh.SkinListCount; j++) {
						skinList[j] = new SkinList();
						IntPtr skinListAddress = cppMesh.pSkinLists + skinListSize * j;
						var cppSkinList = Marshal.PtrToStructure<CPP_SkinList>(cppMesh.pSkinLists);

						skinList[j].BoneInfos = new BoneInfo[cppSkinList.Count];
						int skinSize = Marshal.SizeOf(typeof(CPP_Skin));
						var skinMeshDict = new Dictionary<int, List<WeightInfo>>();
						for (int k = 0; k < cppSkinList.Count; k++) {
							// スキン情報
							IntPtr skinAddress = cppSkinList.pSkins + skinSize * k;
							var cppSkin = Marshal.PtrToStructure<CPP_Skin>(skinAddress);
							int[] indices = new int[cppSkin.Count];
							Marshal.Copy(cppSkin.pIndices, indices, 0, cppSkin.Count);
							float[] weights = new float[cppSkin.Count];
							Marshal.Copy(cppSkin.pWeights, weights, 0, cppSkin.Count);
							for (int l = 0; l < cppSkin.Count; l++) {
								var weightInfo = new WeightInfo();
								weightInfo.BoneIndex = k;
								weightInfo.Weight = weights[l];

								var vertexIndex = indices[l];
								if (!skinMeshDict.ContainsKey(vertexIndex)) {
									skinMeshDict[vertexIndex] = new List<WeightInfo>();
								}
								skinMeshDict[vertexIndex].Add(weightInfo);
							}

							// 骨の姿勢情報
							var boneInfo = new BoneInfo();
							var initMatrix = new float[16];
							Marshal.Copy(cppSkin.pInitMatrix, initMatrix, 0, initMatrix.Length);
							var matrix = new Matrix();
							for (int row = 0; row < 4; row++) {
								var offset = 4 * row;
								matrix.set_Rows(row, new Vector4(initMatrix[offset], initMatrix[offset + 1], initMatrix[offset + 2], initMatrix[offset + 3]));
							}
							boneInfo.BoneIndex = k;
							boneInfo.Name = Marshal.PtrToStringAnsi(cppSkin.pName);

							boneInfo.InitPoseMatrix = matrix;
							matrix.Invert();
							boneInfo.InvInitPoseMatrix = matrix;
							//boneInfo.InvInitPoseMatrix.Invert();
							skinList[j].BoneInfos[k] = boneInfo;
						}
						skinList[j].SkinWeights = skinMeshDict;
					}
					mesh.SkinLists = skinList;
				}
				*/

				// メッシュを格納
				meshes[i] = mesh;
			}

			DllDestroyMeshList(ref cppMeshList);


			// とりあえず最初のメッシュだけ
			//meshes[0].PoseList = CreateAnimationInfo(filePath, 0, 120);



			return meshes;
		}

		#endregion
		public BonePoseList CreateAnimationInfo(string filePath, int startFrame, int frameCount)
		{
			//----------------ボーン
			var cppBonePoseList = new CPP_BonePoseList();
			if (!DllCreateFrameInfo(ref cppBonePoseList, filePath, startFrame, frameCount)) {
				Console.WriteLine("Create FrameInfo Is Failed");
				return null;
			}
			Console.WriteLine("Create FrameInfo Is Succes");
			Console.WriteLine(cppBonePoseList);

			var bonePoseList = new BonePoseList();
			bonePoseList.Poses = new Dictionary<int, Dictionary<string, Matrix>>();
			int bonePoseSize = Marshal.SizeOf(typeof(CPP_BonePose));
			// Pose配列の生成
			for (int i = 0; i < cppBonePoseList.Count; i++) {
				IntPtr adderss = cppBonePoseList.pPoses + bonePoseSize * i;
				var cppPose = Marshal.PtrToStructure<CPP_BonePose>(adderss);

				var pose = new Dictionary<string, Matrix>();
				var frame = cppPose.frameCount;

				var name = Marshal.PtrToStringAnsi(cppPose.pName);

				var boneMatrix = new float[16];
				Marshal.Copy(cppPose.pMatrix, boneMatrix, 0, boneMatrix.Length);
				var matrix = new Matrix();
				for (int row = 0; row < 4; row++) {
					var offset = 4 * row;
					matrix.set_Rows(row, new Vector4(boneMatrix[offset], boneMatrix[offset + 1], boneMatrix[offset + 2], boneMatrix[offset + 3]));
				}
				// キーが無かったら生成
				if (!bonePoseList.Poses.ContainsKey(frame)) {
					bonePoseList.Poses.Add(frame, new Dictionary<string, Matrix>());
				}
				if (!bonePoseList.Poses[frame].ContainsKey(name)) {
					bonePoseList.Poses[frame].Add(name, matrix);
				}
			}
			DllDestroyFrameInfo(ref cppBonePoseList);

			return bonePoseList;
		}
	}


}
