﻿using System.Collections.Generic;
using System.Linq;

namespace WindowsFormsApp1
{
	public class ComponentManager
	{
		private static ComponentManager _instance		= null;
		public static ComponentManager Instance{
			get{
				if (_instance == null) {
					_instance			= new ComponentManager();
				}
				return _instance;
			}
		}

		private List<Component> _components				= new List<Component>();

		public void Add(Component component)
		{
			if (!_components.Contains(component)) {
				_components.Add(component);
			}
		}

		public void Remove(Component component)
		{
			if (_components.Contains(component)) {
				_components.Remove(component);
			}
		}

		public void Update()
		{
			foreach (var component in _components) {
				component.Update();
			}
		}

		public void Draw()
		{
			// ShadowをDraw
			foreach (var component in _components) {
				if (component is DirectionalLight) {
					(component as DirectionalLight).DrawShadow();
				}
			}
			// 場所が不適切だけどとりあえず。。
			Shader.SetGlobalShaderResourceView("_ShadowMap", Graphics.GetShadowMapResourceView());

			// UICamera以外をDraw
			foreach (var component in _components) {
				if (component is Camera && !(component is GizmoCamera) && !(component is UICamera)) {
					(component as Camera).Draw();
				}
			}
			// UICameraをDraw
			foreach (var component in _components) {
				if (component is Camera && !(component is GizmoCamera) && component is UICamera) {
					(component as Camera).Draw();
				}
			}
		}

		public void DrawGizmos()
		{
			//TODO Sceneに表示するアイコンなどのギズモはここで何かしら処理する

			// コンポーネントごとのギズモ処理を登録
			var targets = new List<Component>(_components); // OnDrawGizmosの中でもComponentが作られるのでコピーしておく
			var gizmoCameras = new List<GizmoCamera>(_components.OfType<GizmoCamera>());
			foreach (var component in targets) {
				component.OnDrawGizmos();
			}
			// ギズモを描画
			foreach (var gizmoCamera in gizmoCameras) {
				// ギズモをDraw
				gizmoCamera.Draw();
			}
		}

		public void PostDraw()
		{
			// SwapChainをPresent
			Graphics.PresentSwapChain();
		}

		public T[] GetComponents<T>()
		{
			return _components.OfType<T>().ToArray();
		}
	}
}
