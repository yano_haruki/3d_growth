﻿using SlimDX;
using SlimDX.Direct3D11;
using System.Collections.Generic;
using Device = SlimDX.Direct3D11.Device;

namespace WindowsFormsApp1
{
	public partial class Mesh
	{
		private Vector3[] _vertices;
		public Vector3[] Vertices { get { return _vertices; } set { _vertices = value; } }
		private Color4[] _colors;
		public Color4[] Colors { get { return _colors; } set { _colors = value; } }
		private Vector2[] _texCoords;
		public Vector2[] TexCoords { get { return _texCoords; } set { _texCoords = value; } }
		private int[] _indices;
		public int[] Indices { get { return _indices; } set { _indices = value; } }
		private Vector3[] _normals;
		public Vector3[] Normals { get { return _normals; } set { _normals = value; } }
		private Vector4[] _tangent;
		public Vector4[] Tangents { get { return _tangent; } set { _tangent = value; } }
		public VertexType VertexTypes { get; set; }

		public int VertexCount { get { return _vertices.Length; } }
		public int IndexCount { get { return _indices.Length; } }
		protected Buffer _vertexBuffer;
		private Buffer _indexBuffer;

		public InputElement[] GetInputElements()
		{
			return VertexInfo.GetVertexElement(VertexTypes);
		}

		public int GetVertexSizeInBytes()
		{
			return VertexInfo.GetSizeInBytes(VertexTypes);
		}

		public virtual Buffer GetVertexBuffer(Device device)
		{
			if (_vertexBuffer == null) {
				_vertexBuffer		= CreateVertexBuffer(device);
			}
			return _vertexBuffer;
		}

		public Buffer GetIndexBuffer(Device device)
		{
			if (_indexBuffer == null) {
				_indexBuffer		= CreateIndexBuffer(device);
			}
			return _indexBuffer;
		}

		private Buffer CreateVertexBuffer(Device device)
		{
			var vertexInfos		= new List<VertexInfo>();
			for (int i = 0; i < VertexCount; i++) {
				vertexInfos.Add
				(
					new VertexInfo
					{
						Position	= Vertices[i],
						Color		= Colors != null ? Colors[i] : new Color4(1,1,1,1),
						TexCoord	= TexCoords[i],
						Normal		= Normals != null ? Normals[i] : Vector3.Zero,
						Tangent		= Tangents != null ? Tangents[i] : Vector4.Zero
					}
				);
			}
			using (DataStream vertexStream  = new DataStream(vertexInfos.ToArray(), true, true))
			{
				return new Buffer(
					device,
					vertexStream,
					new BufferDescription
					{
						SizeInBytes= (int)vertexStream.Length,
						BindFlags = BindFlags.VertexBuffer,
					}
				);
			}
		}

		private Buffer CreateIndexBuffer(Device device)
		{
			using (DataStream indexStream  = new DataStream(Indices, false, false))
			{
				return new Buffer(
					device,
					indexStream,
					new BufferDescription
					(
						sizeof(int) * Indices.Length,
						ResourceUsage.Immutable,
						BindFlags.IndexBuffer,
						CpuAccessFlags.None,
						ResourceOptionFlags.None,
						0
					)
				);
			}
		}
	}
}
