﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WindowsFormsApp1
{
	public class GameObject
	{
		public Transform Transform { get; private set; } = new Transform();
		private List<Component> _components			= new List<Component>();

		public GameObject()
		{
			Transform.GameObject	= this;
		}

		public T AddComponent<T>() where T : Component, new()
		{
			var component			= new T();
			component.GameObject	= this;
			component.Transform		= Transform;
			component.Initialize();
			_components.Add(component);
			return component;
		}

		public void RemoveComponent(Component component)
		{
			component.Dispose();
			_components.Remove(component);
		}

		public void Dispose()
		{
			foreach (var component in _components) {
				component.Dispose();
			}
			_components.Clear();
		}
		
		public T GetComponent<T>()
		{
			return _components.OfType<T>().FirstOrDefault();
		}

		public T[] GetComponents<T>()
		{
			return _components.OfType<T>().ToArray();
		}
	}
}