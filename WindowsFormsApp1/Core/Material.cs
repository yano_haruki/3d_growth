﻿using SlimDX;
using SlimDX.D3DCompiler;
using SlimDX.Direct3D11;
using System;
using System.Collections.Generic;
using Device = SlimDX.Direct3D11.Device;

namespace WindowsFormsApp1
{
	public class Material
	{
		public Shader Shader { get; set; }
		private Effect _effect;
		private Dictionary<string, float> _floatProperties					= new Dictionary<string, float>();
		private Dictionary<string, Vector4> _vectorProperties				= new Dictionary<string, Vector4>();
		private Dictionary<string, Color4> _colorProperties					= new Dictionary<string, Color4>();
		private Dictionary<string, Texture2D> _textureProperties			= new Dictionary<string, Texture2D>();
		private Dictionary<string, ShaderResourceView> _shaderResourceViewProperties	= new Dictionary<string, ShaderResourceView>();
		private Dictionary<string, TextureCube> _cubeMapProperties			= new Dictionary<string, TextureCube>();
		private Dictionary<string, Matrix> _matrixProperties					= new Dictionary<string, Matrix>();

		public Material(Shader shader)
		{
			Shader			= shader;
		}

		public Material(Material material)
		{
			// 本来はちゃんとディープコピーしたいところ・・
			// 今のところ用途が影だけなので不具合出たら修正
			Shader = material.Shader;
			_floatProperties = material._floatProperties;// new Dictionary<string, float>(material._floatProperties);
			_vectorProperties = material._vectorProperties;// new Dictionary<string, Vector4>(material._vectorProperties);
			_colorProperties = material._colorProperties;//new Dictionary<string, Color4>(material._colorProperties);
			_textureProperties = material._textureProperties;//new Dictionary<string, Texture2D>(material._textureProperties);
			_shaderResourceViewProperties = material._shaderResourceViewProperties;
			_cubeMapProperties = material._cubeMapProperties;//new Dictionary<string, TextureCube>(material._cubeMapProperties);
			_matrixProperties = material._matrixProperties;//new Dictionary<string, Matrix>(material._matrixProperties);
		}

		public void SetFloat(string name, float value)
		{
			if (!_floatProperties.ContainsKey(name)) {
				_floatProperties.Add(name, value);
			}
			else {
				_floatProperties[name]			= value;
			}
		}

		public void SetVector(string name, Vector2 value)
		{
			SetVector(name, new Vector4(value, 0, 0));
		}
		public void SetVector(string name, Vector3 value)
		{
			SetVector(name, new Vector4(value, 0));
		}
		public void SetVector(string name, Vector4 value)
		{
			if (!_vectorProperties.ContainsKey(name)) {
				_vectorProperties.Add(name, value);
			}
			else {
				_vectorProperties[name]			= value;
			}
		}

		public void SetColor(string name, Color3 value)
		{
			SetColor(name, new Color4(value));
		}
		public void SetColor(string name, Color4 value)
		{
			if (!_colorProperties.ContainsKey(name)) {
				_colorProperties.Add(name, value);
			}
			else {
				_colorProperties[name]			= value;
			}
		}

		public void SetTexture(string name, Texture2D value)
		{
			if (!_textureProperties.ContainsKey(name)) {
				_textureProperties.Add(name, value);
			}
			else {
				_textureProperties[name]			= value;
			}
		}

		public void SetShaderResourceView(string name, ShaderResourceView value)
		{
			if (!_shaderResourceViewProperties.ContainsKey(name)) {
				_shaderResourceViewProperties.Add(name, value);
			}
			else {
				_shaderResourceViewProperties[name]			= value;
			}
		}

		public void SetCubeMap(string name, TextureCube value)
		{
			if (!_cubeMapProperties.ContainsKey(name)) {
				_cubeMapProperties.Add(name, value);
			}
			else {
				_cubeMapProperties[name]			= value;
			}
		}

		public void SetMatrix(string name, Matrix value)
		{
			if (!_matrixProperties.ContainsKey(name)) {
				_matrixProperties.Add(name, value);
			}
			else {
				_matrixProperties[name]			= value;
			}
		}

		public void PrepareDrawing(Device device)
		{
			_effect			= Shader.GetEffect(device);

			// Globalなシェーダプロパティを反映
			foreach (var globalFloat in Shader.GlobalFloats) {
				SetFloatToEffect(device, globalFloat.Key, globalFloat.Value);
			}
			foreach (var globalVector in Shader.GlobalVectors) {
				SetVectorToEffect(device, globalVector.Key, globalVector.Value);
			}
			foreach (var globalColor in Shader.GlobalColors) {
				SetColorToEffect(device, globalColor.Key, globalColor.Value);
			}
			foreach (var globalTexture in Shader.GlobalTextures) {
				SetTextureToEffect(device, globalTexture.Key, globalTexture.Value);
			}
			foreach (var globalShaderResourceView in Shader.GlobalShaderResourceViews) {
				SetShaderResourceViewToEffect(device, globalShaderResourceView.Key, globalShaderResourceView.Value);
			}
			foreach (var globalCubeMap in Shader.GlobalCubeMaps) {
				SetCubeMapToEffect(device, globalCubeMap.Key, globalCubeMap.Value);
			}
			foreach (var globalMatrix in Shader.GlobalMatrix) {
				SetMatrixToEffect(device, globalMatrix.Key, globalMatrix.Value);
			}

			// 設定されているテクスチャをEffectに反映
			foreach (var floatProperty in _floatProperties) {
				SetFloatToEffect(device, floatProperty.Key, floatProperty.Value);
			}
			foreach (var vectorProperty in _vectorProperties) {
				SetVectorToEffect(device, vectorProperty.Key, vectorProperty.Value);
			}
			foreach (var colorProperty in _colorProperties) {
				SetColorToEffect(device, colorProperty.Key, colorProperty.Value);
			}
			foreach (var textureProperty in _textureProperties) {
				SetTextureToEffect(device, textureProperty.Key, textureProperty.Value);
			}
			foreach (var shaderResourceViewProperty in _shaderResourceViewProperties) {
				SetShaderResourceViewToEffect(device, shaderResourceViewProperty.Key, shaderResourceViewProperty.Value);
			}
			foreach (var cubeMapProperty in _cubeMapProperties) {
				SetCubeMapToEffect(device, cubeMapProperty.Key, cubeMapProperty.Value);
			}
			foreach (var matrixProperty in _matrixProperties) {
				SetMatrixToEffect(device, matrixProperty.Key, matrixProperty.Value);
			}

			// ブレンド情報をシェーダから取得して反映する
			var pass				= _effect.GetTechniqueByIndex(0).GetPassByIndex(0);
			var blendSrcStr			= pass.GetAnnotationByName("BlendSrc").AsString();
			var blendDstStr			= pass.GetAnnotationByName("BlendDst").AsString();
			var colorMaskStr		= pass.GetAnnotationByName("ColorMask").AsString();
			var blendSrc			= blendSrcStr == null ? BlendOption.One : GetBlendOptionFromString(blendSrcStr.GetString());
			var blendDst			= blendDstStr == null ? BlendOption.Zero : GetBlendOptionFromString(blendDstStr.GetString());
			var colorMask			= colorMaskStr == null ? ColorWriteMaskFlags.All : GetColorMaskFromString(colorMaskStr.GetString());
			Graphics.SetBlend(blendSrc, blendDst, colorMask);

			// Queueに応じてDepth書き込みを制御
			var renderQueue			= GetRenderQueue(0);
			if (renderQueue == RenderQueue.Depth) {
				Graphics.SetDrawTargetType(Graphics.DrawTargetType.ShadowMap);
			}
			else if (renderQueue == RenderQueue.Geometry) {
				Graphics.SetDrawTargetType(Graphics.DrawTargetType.RenderTargetAndDepth);
			}
			else {
				Graphics.SetDrawTargetType(Graphics.DrawTargetType.RenderTarget);
			}
		}

		public ShaderSignature GetShaderSignature()
		{
			return _effect.GetTechniqueByIndex(0).GetPassByIndex(0).Description.Signature;
		}

		public EffectPass GetEffectPath(Device _device, int techniqueIndex, int pathIndex)
		{
			return _effect.GetTechniqueByIndex(techniqueIndex).GetPassByIndex(pathIndex);
		}

		private RenderQueue GetRenderQueue(int techniqueIndex)
		{
			var queue =  _effect.GetTechniqueByIndex(techniqueIndex).GetAnnotationByName("Queue").AsString();
			if (queue == null) {
				return RenderQueue.Geometry;
			}
			else if (queue.GetString() == "Depth") {
				return RenderQueue.Depth;
			}
			else if (queue.GetString() == "Transparent") {
				return RenderQueue.Transparent;
			}
			return RenderQueue.Geometry;
		}

		private void SetFloatToEffect(Device _device, string name, float value)
		{
			var variable		= _effect.GetVariableByName(name).AsScalar();
			if (variable == null) {
				return;
			}
			variable.Set(value);
		}

		private void SetVectorToEffect(Device _device, string name, Vector2 value)
		{
			SetVectorToEffect(_device, name, new Vector4(value.X, value.Y, 0, 0));
		}

		private void SetVectorToEffect(Device _device, string name, Vector3 value)
		{
			SetVectorToEffect(_device, name, new Vector4(value.X, value.Y, value.Z, 0));
		}

		private void SetVectorToEffect(Device _device, string name, Vector4 value)
		{
			var variable		= _effect.GetVariableByName(name).AsVector();
			if (variable == null) {
				return;
			}
			variable.Set(value);
		}

		private void SetColorToEffect(Device _device, string name, Color3 value)
		{
			SetColorToEffect(_device, name, new Color4(value));
		}
		private void SetColorToEffect(Device _device, string name, Color4 value)
		{
			var variable		= _effect.GetVariableByName(name).AsVector();
			if (variable == null) {
				return;
			}
			variable.Set(value);
		}

		private void SetTextureToEffect(Device _device, string name, Texture2D value)
		{
			var variable		= _effect.GetVariableByName(name).AsResource();
			if (variable == null) {
				return;
			}

			var resourceView		= new ShaderResourceView(_device, value.GetTexture2D(_device));
			variable.SetResource(resourceView);
		}

		private void SetShaderResourceViewToEffect(Device _device, string name, ShaderResourceView value)
		{
			var variable		= _effect.GetVariableByName(name).AsResource();
			if (variable == null) {
				return;
			}

			variable.SetResource(value);
		}

		private void SetCubeMapToEffect(Device _device, string name, TextureCube value)
		{
			var variable		= _effect.GetVariableByName(name).AsResource();
			if (variable == null) {
				return;
			}
			// 必要そうなら書くけど無くても大丈夫そう
			// http://richardssoftware.net/Home/Post/25
			/*
			// ShaderResourceViewDescription
			var shaderResourceViewDesc = new ShaderResourceViewDescription()
			{
				Format = Format.R8G8B8A8_UNorm,
				Dimension = ShaderResourceViewDimension.TextureCube,
				MostDetailedMip = 0,
				MipLevels = -1
			};
			*/
			var resourceView		= new ShaderResourceView(_device, value.GetTexture2D(_device));
			variable.SetResource(resourceView);
		}

		private void SetMatrixToEffect(Device _device, string name, Matrix value)
		{
			var variable		= _effect.GetVariableByName(name).AsMatrix();
			if (variable == null) {
				return;
			}
			variable.SetMatrix(value);
		}

		private BlendOption GetBlendOptionFromString(string blendOptionString)
		{
			foreach (BlendOption blendOption in System.Enum.GetValues(typeof(BlendOption))) {
				if (blendOption.ToString() == blendOptionString) {
					return blendOption;
				}
			}
			throw new Exception("invalid blendOptionString");
		}

		private ColorWriteMaskFlags GetColorMaskFromString(string colorMaskString)
		{
			ColorWriteMaskFlags ret			= ColorWriteMaskFlags.None;
			if (colorMaskString.Contains("R")) {
				ret			&= ~ColorWriteMaskFlags.None;
				ret			|= ColorWriteMaskFlags.Red;
			}
			if (colorMaskString.Contains("G")) {
				ret			&= ~ColorWriteMaskFlags.None;
				ret			|= ColorWriteMaskFlags.Green;
			}
			if (colorMaskString.Contains("B")) {
				ret			&= ~ColorWriteMaskFlags.None;
				ret			|= ColorWriteMaskFlags.Blue;
			}
			if (colorMaskString.Contains("A")) {
				ret			&= ~ColorWriteMaskFlags.None;
				ret			|= ColorWriteMaskFlags.Alpha;
			}
			return ret;
		}
	}
}
