﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;

namespace WindowsFormsApp1
{
	public class TextureCube
	{
		public int Size { get; set; }

		private SlimDX.Direct3D11.Texture2D _texture;
		private byte[] _source;
		
		public TextureCube(byte[] bytres, int size)
		{
			_source = bytres;
			Size = size;
		}
		
		public SlimDX.Direct3D11.Texture2D GetTexture2D(SlimDX.Direct3D11.Device device)
		{
			if (_texture != null) {
				return _texture;
			}
		
			/*
			var loadInfo = new SlimDX.Direct3D11.ImageLoadInformation();
			loadInfo.OptionFlags = SlimDX.Direct3D11.ResourceOptionFlags.TextureCube;
			*/
			_texture		= SlimDX.Direct3D11.Texture2D.FromMemory(device, _source);

			return _texture;
		}
	}
}
