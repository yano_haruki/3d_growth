﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX.RawInput;
using SlimDX;
using SlimDX.Multimedia;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
	public class Input
	{
		public enum MouseKeys
		{
			Left,
			Right,
			Middle
		}

		private static Input _instance;
		public static Input Instance { get { return _instance ?? (_instance = new Input()); } }
		public Vector2 ScreenMousePosition { get; set; } // マウス座標はWindows.Formsの物を使うので外側から入力する
		public Vector2 ViewportMousePosition { get { return new Vector2(ScreenMousePosition.X / ScreenUtility.Width, ScreenMousePosition.Y / ScreenUtility.Height); } }

		private List<Keys> _pressedKeys					= new List<Keys>();
		private List<Keys> _pressingKeys				= new List<Keys>();
		private List<Keys> _releasedKeys				= new List<Keys>();
		private List<MouseKeys> _pressedMouseKeys		= new List<MouseKeys>();
		private List<MouseKeys> _pressingMouseKeys		= new List<MouseKeys>();
		private List<MouseKeys> _releasedMouseKeys		= new List<MouseKeys>();
		private Vector2 _mouseDelta						= new Vector2();
		private float _wheelDelta						= 0.0f;

		public void Setup()
		{
			Device.RegisterDevice(UsagePage.Generic, UsageId.Keyboard, DeviceFlags.None);
			Device.KeyboardInput	+= (sender, e) => {
				if (e.State == KeyState.Pressed) {
					if (!_pressedKeys.Contains(e.Key)) {
						_pressedKeys.Add(e.Key);
					}
					if (!_pressingKeys.Contains(e.Key)) {
						_pressingKeys.Add(e.Key);
					}
				}
				if (e.State == KeyState.Released) {
					if (!_releasedKeys.Contains(e.Key)) {
						_releasedKeys.Add(e.Key);
					}
					if (_pressingKeys.Contains(e.Key)) {
						_pressingKeys.Remove(e.Key);
					}
				}
			};

			Device.RegisterDevice(UsagePage.Generic, UsageId.Mouse, DeviceFlags.None);
			Device.MouseInput		+= (sender, e) => {
				switch (e.ButtonFlags) {
				case MouseButtonFlags.LeftUp:
					if (!_releasedMouseKeys.Contains(MouseKeys.Left)) {
						_releasedMouseKeys.Add(MouseKeys.Left);
					}
					if (_pressingMouseKeys.Contains(MouseKeys.Left)) {
						_pressingMouseKeys.Remove(MouseKeys.Left);
					}
					break;
				case MouseButtonFlags.LeftDown:
					if (!_pressedMouseKeys.Contains(MouseKeys.Left)) {
						_pressedMouseKeys.Add(MouseKeys.Left);
					}
					if (!_pressingMouseKeys.Contains(MouseKeys.Left)) {
						_pressingMouseKeys.Add(MouseKeys.Left);
					}
					break;
				case MouseButtonFlags.RightUp:
					if (!_releasedMouseKeys.Contains(MouseKeys.Right)) {
						_releasedMouseKeys.Add(MouseKeys.Right);
					}
					if (_pressingMouseKeys.Contains(MouseKeys.Right)) {
						_pressingMouseKeys.Remove(MouseKeys.Right);
					}
					break;
				case MouseButtonFlags.RightDown:
					if (!_pressedMouseKeys.Contains(MouseKeys.Right)) {
						_pressedMouseKeys.Add(MouseKeys.Right);
					}
					if (!_pressingMouseKeys.Contains(MouseKeys.Right)) {
						_pressingMouseKeys.Add(MouseKeys.Right);
					}
					break;
				case MouseButtonFlags.MiddleUp:
					if (!_releasedMouseKeys.Contains(MouseKeys.Middle)) {
						_releasedMouseKeys.Add(MouseKeys.Middle);
					}
					if (_pressingMouseKeys.Contains(MouseKeys.Middle)) {
						_pressingMouseKeys.Remove(MouseKeys.Middle);
					}
					break;
				case MouseButtonFlags.MiddleDown:
					if (!_pressedMouseKeys.Contains(MouseKeys.Middle)) {
						_pressedMouseKeys.Add(MouseKeys.Middle);
					}
					if (!_pressingMouseKeys.Contains(MouseKeys.Middle)) {
						_pressingMouseKeys.Add(MouseKeys.Middle);
					}
					break;
				default:
					break;
				}
				
				_mouseDelta.X				+= e.X;
				_mouseDelta.Y				+= e.Y;
				_wheelDelta					+= e.WheelDelta;
			};
		}
		
		public bool GetKey(Keys key)
		{
			return _pressingKeys.Contains(key);
		}

		public bool GetKeyDown(Keys key)
		{
			return _pressedKeys.Contains(key);
		}

		public bool GetKeyUp(Keys key)
		{
			return _releasedKeys.Contains(key);
		}

		public bool GetMouse(MouseKeys key)
		{
			return _pressingMouseKeys.Contains(key);
		}

		public bool GetMouseDown(MouseKeys key)
		{
			return _pressedMouseKeys.Contains(key);
		}

		public bool GetMouseUp(MouseKeys key)
		{
			return _releasedMouseKeys.Contains(key);
		}

		public Vector2 GetMouseDelta()
		{
			return _mouseDelta;
		}

		public void LateUpdate()
		{
			_pressedKeys.Clear();
			_releasedKeys.Clear();
			_pressedMouseKeys.Clear();
			_releasedMouseKeys.Clear();
			_mouseDelta.X		= 0.0f;
			_mouseDelta.Y		= 0.0f;
			_wheelDelta			= 0.0f;
		}
	}
}
