﻿using SlimDX;
using SlimDX.D3DCompiler;
using SlimDX.Direct3D11;
using System.Collections.Generic;
using Device = SlimDX.Direct3D11.Device;

namespace WindowsFormsApp1
{
	public class Shader
	{
		private byte[] _source;
		public static Dictionary<string, float> GlobalFloats { get; private set; } = new Dictionary<string, float>();
		public static Dictionary<string, Vector4> GlobalVectors { get; private set; } = new Dictionary<string, Vector4>();
		public static Dictionary<string, Color4> GlobalColors { get; private set; } = new Dictionary<string, Color4>();
		public static Dictionary<string, Texture2D> GlobalTextures { get; private set; } = new Dictionary<string, Texture2D>();
		public static Dictionary<string, ShaderResourceView> GlobalShaderResourceViews { get; private set; } = new Dictionary<string, ShaderResourceView>();
		public static Dictionary<string, TextureCube> GlobalCubeMaps { get; private set; } = new Dictionary<string, TextureCube>();
		public static Dictionary<string, Matrix> GlobalMatrix { get; private set; } = new Dictionary<string, Matrix>();

		private Effect _compiled = null;

		public static void SetGlobalFloat(string name, float value)
		{
			if (GlobalFloats.ContainsKey(name)) {
				GlobalFloats[name]		= value;
			}
			else {
				GlobalFloats.Add(name, value);
			}
		}

		public static void SetGlobalVector(string name, Vector2 value)
		{
			SetGlobalVector(name, new Vector4(value, 0.0f, 0.0f));
		}
		public static void SetGlobalVector(string name, Vector3 value)
		{
			SetGlobalVector(name, new Vector4(value, 0.0f));
		}
		public static void SetGlobalVector(string name, Vector4 value)
		{
			if (GlobalVectors.ContainsKey(name)) {
				GlobalVectors[name]		= value;
			}
			else {
				GlobalVectors.Add(name, value);
			}
		}

		public static void SetGlobalColor(string name, Color3 value)
		{
			SetGlobalColor(name, new Color4(value));
		}
		public static void SetGlobalColor(string name, Color4 value)
		{
			if (GlobalColors.ContainsKey(name)) {
				GlobalColors[name]		= value;
			}
			else {
				GlobalColors.Add(name, value);
			}
		}

		public static void SetGlobalTexture(string name, Texture2D value)
		{
			if (GlobalTextures.ContainsKey(name)) {
				GlobalTextures[name]		= value;
			}
			else {
				GlobalTextures.Add(name, value);
			}
		}

		public static void SetGlobalShaderResourceView(string name, ShaderResourceView value)
		{
			if (GlobalShaderResourceViews.ContainsKey(name)) {
				GlobalShaderResourceViews[name]		= value;
			}
			else {
				GlobalShaderResourceViews.Add(name, value);
			}
		}

		public static void SetGlobalCubeMap(string name, TextureCube value)
		{
			if (GlobalCubeMaps.ContainsKey(name)) {
				GlobalCubeMaps[name]		= value;
			}
			else {
				GlobalCubeMaps.Add(name, value);
			}
		}

		public static void SetGlobalMatrix(string name, Matrix value)
		{
			if (GlobalMatrix.ContainsKey(name)) {
				GlobalMatrix[name]		= value;
			}
			else {
				GlobalMatrix.Add(name, value);
			}
		}

		public Shader(byte[] shaderSource)
		{
			_source = shaderSource;
		}

		public Effect GetEffect(Device device)
		{
			if (_compiled == null) {
				using (var bytecode = ShaderBytecode.Compile(_source, "fx_5_0", ShaderFlags.None, EffectFlags.None))
				{
					_compiled = new Effect(device, bytecode);
				}
			}
			return _compiled;
		}
	}
}
