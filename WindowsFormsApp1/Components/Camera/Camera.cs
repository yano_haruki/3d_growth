﻿using SlimDX;
using System.Linq;

namespace WindowsFormsApp1
{
	public enum CameraClearFlags
	{
		DontClear,
		SolidColor
	}

	public abstract class Camera : Component
	{
		public static Camera Main { get; set; }

		public CameraClearFlags ClearFlag { get; set; }	= CameraClearFlags.SolidColor;
		public Color4 ClearColor { get; set; }		= new Color4(1.0f, 0.5f, 0.5f, 0.5f);

		public abstract void Draw();

		public Vector3 ViewportPointToWorldPoint(Vector2 viewportPoint)
		{
			// -1から1に変換
			viewportPoint = Vector2.Multiply(viewportPoint - new Vector2(0.5f, 0.5f), 2.0f);
			// w値を掛ける
			viewportPoint = Vector2.Multiply(viewportPoint, GetMatrixVP().M44);
			// VP行列の逆行列を掛ける
			var viewportPoint3 = new Vector3(viewportPoint.X, viewportPoint.Y, 0);
			var matrix = GetMatrixVP();
			matrix.Invert();
			return Vector3.Transform(viewportPoint3, matrix).ToVector3();
		}

		public Ray ViewportPointToRay(Vector2 viewportPoint)
		{
			var ray = new Ray();
			ray.Origin = ViewportPointToWorldPoint(viewportPoint);
			ray.Direction = Transform.Forward;
			return ray;
		}

		public virtual Matrix GetMatrixVP()
		{
			return GetMatrixV() * GetMatrixP();
		}

		protected abstract Matrix GetMatrixV();

		protected abstract Matrix GetMatrixP();
	}

	public abstract class Camera<TRenderer> : Camera
	{

		public override void Draw()
		{
			Graphics.SetMatrixVP(GetMatrixVP());

			// Set Viewport
			Graphics.SetViewport(false);

			// Clear
			if (ClearFlag == CameraClearFlags.SolidColor) {
				Graphics.Clear(ClearColor, false);
			}

			// Draw
			var renderers		= ComponentManager
				.Instance
				.GetComponents<IRenderer>()
				.Where(x => x is TRenderer);
			// TODO　位置によるソート
			foreach (var renderer in renderers) {
				renderer.Draw();
			}
		}


		/*
		public virtual void DrawGizmos()
		{
			Graphics.SetMatrixVP(GetMatrixVP());

			// Draw
			var renderers		= ComponentManager
				.Instance
				.GetComponents<IRenderer>()
				.Where(x => x is TRenderer);
			// ギズモ描画
			if (Graphics.DebugMode == Graphics.DebugModeType.Wireframe) {
				Graphics.SetFillMode(FillMode.Wireframe);
				// ワイヤフレームを描画
				foreach (var renderer in renderers) {
					var meshRenderer	= renderer as MeshRenderer;
					if (meshRenderer != null) {
						// ワイヤフレーム用のシェーダに差し替えて描画
						//TODO アドホックなのでこれ以上要件が増える場合はリファクタ
						var preShader		= meshRenderer.Material.Shader;
						meshRenderer.Material.Shader		= new Shader(Properties.Resources.shader_gizmo_outline);
						meshRenderer.Draw();
						meshRenderer.Material.Shader		= preShader;
					}
				}
				Graphics.SetFillMode(FillMode.Solid);
			}
			foreach (var renderer in renderers) {
				renderer.OnDrawGizmos();
			}
		}
		*/
	}
}
