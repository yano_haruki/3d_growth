﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SlimDX;

namespace WindowsFormsApp1
{
	class FpsCameraController : Component
	{
		private const float MOVE_SPEED_FACTOR	= 0.02f;

		public override void Update()
		{
			if (Input.Instance.GetKey(Keys.W)) {
				// 前進する
				Transform.LocalPosition += Transform.Forward * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetKey(Keys.S)) {
				// 後退する
				Transform.LocalPosition -= Transform.Forward * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetKey(Keys.A)) {
				// 左に移動
				Transform.LocalPosition -= Transform.Right * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetKey(Keys.D)) {
				// 右に移動
				Transform.LocalPosition += Transform.Right * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetMouse(Input.MouseKeys.Left)) {
				var mouseDelta = Input.Instance.GetMouseDelta() * 8;

				var eulerAngleX						= Transform.LocalEulerAngles.X + mouseDelta.Y * 0.01f * Time.DeltaTime;
				eulerAngleX							= Math.Min(89 * Mathf.Deg2Rad, Math.Max(-89 * Mathf.Deg2Rad, eulerAngleX));
				var eulerAngleY						= Transform.LocalEulerAngles.Y + mouseDelta.X * 0.01f * Time.DeltaTime;
				Transform.LocalEulerAngles = new Vector3(eulerAngleX, eulerAngleY, Transform.LocalEulerAngles.Z);
			}
		}
	}
}
