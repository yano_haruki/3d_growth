﻿using SlimDX;
using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
	class LookCenterCameraController : Component
	{
		private const float MOVE_SPEED_FACTOR	= 0.5f;
		private Transform _centerTransform;

		/// <summary>
		/// 初期化
		/// </summary>
		public void Setup()
		{
			var parent = new GameObject();
			Transform.Parent = parent.Transform;
			_centerTransform = parent.Transform;
		}

		public override void Update()
		{
			if (Input.Instance.GetKey(Keys.W)) {
				// 前進する
				var vector = _centerTransform.Position - Transform.Position;
				var length = vector.Length();
				if (length >= 1) {
					// 距離が一定以上遠い場合のみ近づける
					Transform.LocalPosition += Vector3.UnitZ * MOVE_SPEED_FACTOR;
				}
			}
			if (Input.Instance.GetKey(Keys.S)) {
				// 後退する
				var vector = _centerTransform.Position - Transform.Position;
				Transform.LocalPosition -= Vector3.UnitZ * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetMouse(Input.MouseKeys.Left)) {
				var mouseDelta = Input.Instance.GetMouseDelta() * 4;

				var eulerAngleX						= _centerTransform.LocalEulerAngles.X + mouseDelta.Y * 0.01f * Time.DeltaTime;
				eulerAngleX							= Math.Min(89 * Mathf.Deg2Rad, Math.Max(-89 * Mathf.Deg2Rad, eulerAngleX));
				var eulerAngleY						= _centerTransform.LocalEulerAngles.Y + mouseDelta.X * 0.01f * Time.DeltaTime;
				_centerTransform.LocalEulerAngles = new Vector3(eulerAngleX, eulerAngleY, _centerTransform.LocalEulerAngles.Z);
			}
		}
	}
}
