﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX.Direct3D11;
using SlimDX;

namespace WindowsFormsApp1
{
	public abstract class GizmoCamera : Camera
	{
		public override void Draw()
		{
			Graphics.SetMatrixVP(GetMatrixVP());

			// ワイヤフレーム表示
			Graphics.SetFillMode(FillMode.Wireframe);

			// Draw
			foreach (var renderer in Gizmos.Renderers) {
				renderer.Draw();
			}
			
			Graphics.SetFillMode(FillMode.Solid);
			Gizmos.ClearRenderer();
		}
	}
}
