﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX.Direct3D11;
using SlimDX;

namespace WindowsFormsApp1
{

	public class PerspectiveGizmoCamera : GizmoCamera
	{
		public float Fov { get; set; } = 60 * Mathf.Deg2Rad;
		public float ZNear { get; set; } = 0.1f;
		public float ZFar { get; set; } = 1000.0f;

		protected override Matrix GetMatrixV()
		{	
			return Matrix.LookAtLH(Transform.Position, Transform.Position + Transform.Forward, Transform.Up);
		}

		protected override Matrix GetMatrixP()
		{
			return Matrix.PerspectiveFovLH(Fov, (float)ScreenUtility.Width / (float)ScreenUtility.Height, ZNear, ZFar);
		}

		/*
		public virtual void DrawGizmos()
		{
			Graphics.SetMatrixVP(GetMatrixVP());
			
			// Draw
			var renderers		= ComponentManager
				.Instance
				.GetComponents<IRenderer>()
				.Where(x => x is TRenderer);
			// ギズモ描画
			if (Graphics.DebugMode == Graphics.DebugModeType.Wireframe) {
				Graphics.SetFillMode(FillMode.Wireframe);
				// ワイヤフレームを描画
				foreach (var renderer in renderers) {
					var meshRenderer	= renderer as MeshRenderer;
					if (meshRenderer != null) {
						// ワイヤフレーム用のシェーダに差し替えて描画
						//TODO アドホックなのでこれ以上要件が増える場合はリファクタ
						var preShader		= meshRenderer.Material.Shader;
						meshRenderer.Material.Shader		= new Shader(Properties.Resources.shader_gizmo_outline);
						meshRenderer.Draw();
						meshRenderer.Material.Shader		= preShader;
					}
				}
				Graphics.SetFillMode(FillMode.Solid);
			}
			foreach (var renderer in renderers) {
				renderer.OnDrawGizmos();
			}
		}
		*/
	}
}
