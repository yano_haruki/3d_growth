﻿using SlimDX;

namespace WindowsFormsApp1
{
	public class PerspectiveCamera : Camera<MeshRenderer>
	{
		private float _fov = 60 * Mathf.Deg2Rad;
		public float Fov {
			get { return _fov; }
			set {
				_fov = value;
				if (_gizmoCamera != null) {
					_gizmoCamera.Fov = value;
				}
			}
		}
		private float _zNear = 0.1f;
		public float ZNear {
			get { return _zNear; }
			set {
				_zNear = value;
				if (_gizmoCamera != null) {
					_gizmoCamera.ZNear = value;
				}
			}
		}
		private float _zFar = 12000.0f;
		public float ZFar {
			get { return _zFar; }
			set {
				_zFar = value;
				if (_gizmoCamera != null) {
					_gizmoCamera.ZFar = value;
				}
			}
		}
		private PerspectiveGizmoCamera _gizmoCamera;

		//TODO ベースクラスにabstract定義する？
		public void SetupGizmoCamera()
		{
			_gizmoCamera = new GameObject().AddComponent<PerspectiveGizmoCamera>();
			_gizmoCamera.Transform.Parent = Transform;
		}

		protected override Matrix GetMatrixV()
		{
			Shader.SetGlobalVector("_WorldSpaceCameraPos", Transform.Position); // TODO ここでやるべきじゃないのでうつす

			return Matrix.LookAtLH(Transform.Position, Transform.Position + Transform.Forward, Transform.Up);
		}

		protected override Matrix GetMatrixP()
		{
			return Matrix.PerspectiveFovLH(Fov, (float)ScreenUtility.Width / (float)ScreenUtility.Height, ZNear, ZFar);
		}
	}
}
