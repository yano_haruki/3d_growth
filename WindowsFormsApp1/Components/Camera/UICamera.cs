﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	class UICamera : Camera<UIRenderer>
	{
		protected override Matrix GetMatrixV()
		{
			return Matrix.Identity;
		}

		protected override Matrix GetMatrixP()
		{
			return Matrix.Identity;
		}
	}
}
