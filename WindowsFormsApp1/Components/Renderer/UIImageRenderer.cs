﻿using SlimDX;
using System.Collections.Generic;

namespace WindowsFormsApp1
{
	public class UIImageRenderer : UIRenderer
	{
		public Color4 TintColor { get; set; }			= new Color4(1, 1, 1);
		public Texture2D Texture { get; set; }
		public Vector2 ViewportSize { get; set; }		= new Vector2(1, 1);

		protected override Material CreateMaterial()
		{
			if (Texture == null) {
				Texture		= new Texture2D(Properties.Resources.tex_clear);
			}
			var material		= new Material(new Shader(Properties.Resources.shader_ui_default));
			material.SetTexture("_MainTex", Texture);
			return material;
		}

		protected override Mesh CreateMesh()
		{
			var mesh		= new Mesh();
			var minPos		= (ViewportPosition - new Vector2(0.5f, 0.5f)) * 2;
			var size		= ViewportSize * 2;
			var maxPos		= minPos + size;
			var normal		= Vector3.UnitZ;
			mesh.Vertices			= new List<Vector3>()
			{
				new Vector3(minPos.X, minPos.Y, 0.0f),
				new Vector3(maxPos.X, minPos.Y, 0.0f),
				new Vector3(minPos.X, maxPos.Y, 0.0f),
				new Vector3(maxPos.X, maxPos.Y, 0.0f)

			}
			.ToArray();
			mesh.Indices		= new List<int>() { 0, 2, 1, 1, 2, 3 }.ToArray();
			mesh.Colors			= new List<Color4>() { TintColor, TintColor, TintColor, TintColor }.ToArray();
			mesh.Normals		= new List<Vector3>() { normal, normal, normal, normal }.ToArray();
			mesh.TexCoords		= new List<Vector2>() { new Vector2(0.0f, 1.0f), new Vector2(1.0f, 1.0f), new Vector2(0.0f, 0.0f), new Vector2(1.0f, 0.0f) }.ToArray();

			mesh.VertexTypes = VertexType.Position | VertexType.TexCoord | VertexType.Color | VertexType.Normal;

			return mesh;
		}
	}
}
