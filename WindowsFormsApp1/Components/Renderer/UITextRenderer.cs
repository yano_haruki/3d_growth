﻿using SlimDX;
using System;
using System.Collections.Generic;

namespace WindowsFormsApp1
{
	class UITextRenderer : UIRenderer
	{
		public enum SizeFitMode
		{
			UseRawSize,
			FitWidthToHeight,
			FitHeightToWidth
		}

		public enum VerticalPivotType
		{
			Top,
			Middle,
			Bottom
		}

		public enum HorizontalPivotType
		{
			Left,
			Center,
			Right
		}

		public Color4 Color { get; set; }			= new Color4(1, 1, 1);
		public FontType FontType { get; set; }		= FontType.Gothic;
		public float Size { get; set; }				= 1.0f;
		public string Text { get; set; }			= "";
		public VerticalPivotType VerticalPivot { get; set; } = VerticalPivotType.Bottom;
		public HorizontalPivotType HorizontalPivot { get; set; } = HorizontalPivotType.Left;

		protected override Material CreateMaterial()
		{
			var texture			= new Texture2D(FontType.GetBitmap());
			var material		= new Material(new Shader(Properties.Resources.shader_ui_text));
			material.SetTexture("_MainTex", texture);
			return material;
		}

		protected override Mesh CreateMesh()
		{
			var mesh				= new Mesh();
			var vertices			= new List<Vector3>();
			var colors				= new List<Color4>();
			var texCoords			= new List<Vector2>();
			var normals				= new List<Vector3>();
			var indices				= new List<int>();

			for (int i = 0; i < Text.Length; i++) {
				var character		= Text[i];

				var viewportRect	= GetViewportRect(i);
				var minPos			= (viewportRect.Min - new Vector2(0.5f, 0.5f)) * 2;
				var maxPos			= (viewportRect.Max - new Vector2(0.5f, 0.5f)) * 2;
				vertices.Add(new Vector3(minPos.X, minPos.Y, 0.0f));
				vertices.Add(new Vector3(maxPos.X, minPos.Y, 0.0f));
				vertices.Add(new Vector3(minPos.X, maxPos.Y, 0.0f));
				vertices.Add(new Vector3(maxPos.X, maxPos.Y, 0.0f));

				// UV, Color
				var uv				= GetTexCoord(character);
				if (uv == null) {
					// 不正なアスキーコードの場合は適当なUVを入れたうえで透明にする
					uv		= new List<Vector2>() { new Vector2(0.0f, 1.0f), new Vector2(1.0f, 1.0f), new Vector2(0.0f, 0.0f), new Vector2(1.0f, 0.0f) };
					var transparent		= new Color4(0, 0, 0, 0);
					colors.AddRange(new List<Color4>() { transparent, transparent, transparent, transparent });
				}
				else {
					colors.AddRange(new List<Color4>() { Color, Color, Color, Color }.ToArray());
				}
				texCoords.AddRange(uv);

				// Normal
				normals.Add(Vector3.UnitZ);
				normals.Add(Vector3.UnitZ);
				normals.Add(Vector3.UnitZ);
				normals.Add(Vector3.UnitZ);

				// indices
				var indexOffset		= i * 4;
				indices.AddRange(new List<int>() { indexOffset, indexOffset + 2, indexOffset + 1, indexOffset + 1, indexOffset + 2, indexOffset + 3 });
			}
			mesh.Vertices			= vertices.ToArray();
			mesh.Colors				= colors.ToArray();
			mesh.TexCoords			= texCoords.ToArray();
			mesh.Normals			= normals.ToArray();
			mesh.Indices			= indices.ToArray();

			mesh.VertexTypes = VertexType.Position | VertexType.TexCoord | VertexType.Color | VertexType.Normal;

			return mesh;
		}

		/// <summary>
		/// 当該文字の表示される領域をビューポート座標で得る
		/// </summary>
		private Rect GetViewportRect(int index)
		{
			var rect		= new Rect();
			rect.Min		= ViewportPosition;
			var scale		= Size / 10.0f;
			rect.Width		= scale * (float)ScreenUtility.Height / ScreenUtility.Width;
			rect.Height		= scale;
			rect.MinX		+= index * rect.Width;

			// Vertical Pivotを適用
			switch (VerticalPivot) {
			case VerticalPivotType.Top:
				rect.MinY		-= GetViewportSize().Y;
				break;
			case VerticalPivotType.Middle:
				rect.MinY		-= GetViewportSize().Y / 2;
				break;
			case VerticalPivotType.Bottom:
				break;
			default:
				break;
			}

			// Horizontal Pivotを適用
			switch (HorizontalPivot) {
			case HorizontalPivotType.Left:
				break;
			case HorizontalPivotType.Center:
				rect.MinX		-= GetViewportSize().X / 2;
				break;
			case HorizontalPivotType.Right:
				rect.MinX		-= GetViewportSize().X;
				break;
			default:
				break;
			}
			return rect;
		}

		/// <summary>
		/// 表示される領域のサイズをビューポート座標系で得る
		/// </summary>
		private Vector2 GetViewportSize()
		{
			var size		= new Vector2();
			var scale		= Size / 10.0f;
			size.X			= scale * (float)ScreenUtility.Height / ScreenUtility.Width * Text.Length;
			size.Y			= scale;
			return size;
		}

		private List<Vector2> GetTexCoord(char character)
		{
			// 大文字、数字、記号のみ収録
			// アスキーコードのマッピング
			// 32 ... 46
			// 47 ... 61
			// 62 ... 76
			// 77 ... 90

			// 小文字は使えないので大文字にする
			character			= character.ToString().ToUpper().ToCharArray()[0];

			// アスキーコード
			int code			= character;

			if (code < 32 || code > 90) {
				// 不正なアスキーコードの場合はnull
				return null;
			}
			else {
				var rowCount		= 4;
				var columnCount		= 15;
				// 0～58に変換
				// 0 ... 14
				// 15 ... 29
				// 30 ... 44
				// 45 ... 58
				code				-= 32;
				// 行と列を求める
				var columnIndex		= code % columnCount;
				var rowIndex		= Math.Floor((double)code / columnCount);
				// UVの最大値を指定
				var maxUv			= new Vector2(0.965f, 0.5f);
				var u				= (float)columnIndex / columnCount * maxUv.X;
				var v				= (float)rowIndex / rowCount * maxUv.Y;
				var width			= maxUv.X / columnCount;
				var height			= maxUv.Y / rowCount;

				return new List<Vector2>() { new Vector2(u, v + height), new Vector2(u + width, v + height), new Vector2(u, v), new Vector2(u + width, v) };
			}
		}
	}
}
