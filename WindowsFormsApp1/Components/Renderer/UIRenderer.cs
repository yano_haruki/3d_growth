﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	public abstract class UIRenderer : Component, IRenderer
	{
		protected Mesh _mesh;
		protected Material _material;
		public Material Material { get { return _material; } set { _material = value; } }
		public Vector2 ViewportPosition { get; set; }	= new Vector2(0, 0);
		
		protected abstract Material CreateMaterial();

		public void Draw()
		{
			if (_mesh == null) {
				_mesh		= CreateMesh();
			}
			if (_material == null) {
				_material	= CreateMaterial();
			}
			Graphics.DrawMesh(_mesh, _material, Matrix.Identity);
		}

		protected abstract Mesh CreateMesh();
	}
}
