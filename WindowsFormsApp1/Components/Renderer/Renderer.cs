﻿namespace WindowsFormsApp1
{
	public interface IRenderer
	{
		Material Material { get; set; }

		void Draw();
	}

	public interface IShadowRenderer
	{
		Material ShadowMaterial { get; }

		void DrawShadow();
	}
}
