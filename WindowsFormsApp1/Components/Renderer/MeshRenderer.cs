﻿namespace WindowsFormsApp1
{
	public class MeshRenderer : Component, IRenderer, IShadowRenderer
	{
		public Mesh Mesh { get; set; }
		private Material _material;
		public Material Material {
			get { return _material; }
			set {
				_material = value;
				ShadowMaterial = new Material(value);
				ShadowMaterial.Shader = new Shader(Properties.Resources.shader_build_shadow_map);
			}
		}
		public Material ShadowMaterial { get; private set; }

		public void DrawShadow()
		{
			Graphics.DrawShadow(Mesh, ShadowMaterial, Transform.LocalToWorldMatrix);
		}

		public void Draw()
		{
			Graphics.DrawMesh(Mesh, Material, Transform.LocalToWorldMatrix);
		}
	}
}
