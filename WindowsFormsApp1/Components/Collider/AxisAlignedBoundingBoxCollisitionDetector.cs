﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	class AxisAlignedBoundingBoxCollisitionDetector : CollisionDetectorBase, IAxisAlignedBoundngBox
	{
		/// <summary>
		/// 中心のローカル座標
		/// </summary>
		public Vector3 Center { get; set; } = new Vector3(0, 0, 0);
		/// <summary>
		/// 中心のワールド座標
		/// </summary>
		public Vector3 WorldCenter { get { return Transform.Position + Center; } }
		/// <summary>
		/// 各軸ごとのサイズ
		/// </summary>
		public Vector3 Size { get; set; } = new Vector3(1, 1, 1);
		/// <summary>
		/// 親もふまえたサイズ
		/// </summary>
		public Vector3 LossySize { 
			get
			{
				return Transform.LossyScale.Multiply(Size);
			}
		}
		/// <summary>
		/// 最少座標
		/// </summary>
		public Vector3 WorldMin { 
			get{
				return WorldCenter - LossySize / 2;
			}
		}
		/// <summary>
		/// 最大座標
		/// </summary>
		public Vector3 WorldMax {
			get{
				return WorldCenter + LossySize / 2;
			}
		}

		protected override bool Detect(ICollider target)
		{
			return target.CheckAxisAlignedBoundingBox(this);
		}
	}
}
