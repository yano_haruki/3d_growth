﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	class CapsuleCollider : Component, ICollider, ICapsule
	{
		public Vector3 Center { get; set; } = Vector3.Zero;
		private float _radius = 0.5f;
		public float Radius { get { return _radius * Transform.LocalScale.GetMaxElement(); } set { _radius = value; } }
		public float Height { get; } = 1.0f;
		public Vector3 StartPoint { get { return Center - Vector3.UnitY * Height * 0.5f; } }
		public Vector3 EndPoint { get { return Center + Vector3.UnitY * Height * 0.5f; } }
		public Vector3 WorldStartPoint { get { return Vector3.Transform(StartPoint, Transform.LocalToWorldMatrix).ToVector3(); } }
		public Vector3 WorldEndPoint { get { return Vector3.Transform(EndPoint, Transform.LocalToWorldMatrix).ToVector3(); } }

		/// <summary>
		/// 球との当たり判定
		/// </summary>
		public bool CheckSphere(ISphere collider)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// カプセルとの当たり判定
		/// </summary>
		public bool CheckCapsule(ICapsule collider)
		{
			var distance = Math3D.SegmentSegmentDistance(WorldStartPoint, WorldEndPoint, collider.WorldStartPoint, collider.WorldEndPoint);
			return distance <= Radius + collider.Radius;
		}
		
		/// <summary>
		/// 直方体との当たり判定
		/// </summary>
		public bool CheckAxisAlignedBoundingBox(IAxisAlignedBoundngBox collider)
		{
			throw new NotImplementedException();
		}

		public override void OnDrawGizmos()
		{
			base.OnDrawGizmos();
		}
	}
}
