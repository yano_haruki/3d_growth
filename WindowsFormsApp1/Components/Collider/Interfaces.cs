﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	public interface ICollider
	{
		/// <summary>
		/// 球との当たり判定
		/// </summary>
		bool CheckSphere(ISphere collider);

		/// <summary>
		/// カプセルとの当たり判定
		/// </summary>
		bool CheckCapsule(ICapsule collider);

		/// <summary>
		/// AABBとの当たり判定
		/// </summary>
		bool CheckAxisAlignedBoundingBox(IAxisAlignedBoundngBox collider);
	}

	public interface ISphere
	{
		/// <summary>
		/// 中心のローカル座標
		/// </summary>
		Vector3 Center { get; }
		/// <summary>
		/// 半径
		/// </summary>
		float Radius { get; }
		/// <summary>
		/// 中心のワールド座標
		/// </summary>
		Vector3 WorldCenter { get; }
	}

	public interface ICapsule
	{
		/// <summary>
		/// 中心のローカル座標
		/// </summary>
		Vector3 Center { get; }
		/// <summary>
		/// 半径
		/// </summary>
		float Radius { get; }
		/// <summary>
		/// 高さ
		/// </summary>
		float Height { get; }
		/// <summary>
		/// ワールド空間における始点
		/// </summary>
		Vector3 WorldStartPoint { get; }
		/// <summary>
		/// ワールド空間における終点
		/// </summary>
		Vector3 WorldEndPoint { get; }
	}

	public interface IAxisAlignedBoundngBox
	{
		/// <summary>
		/// 最少座標
		/// </summary>
		Vector3 WorldMin { get; }
		/// <summary>
		/// 最大座標
		/// </summary>
		Vector3 WorldMax { get; }
	}
}
