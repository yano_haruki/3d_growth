﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	abstract class CollisionDetectorBase : Component
	{
		public List<ICollider> Targets { get; set; } = new List<ICollider>();
		
		public event Action<ICollider> onCollisionEnter;
		public event Action<ICollider> onCollisionExit;
		public event Action<ICollider> onCollisionStay;
		public bool IsDebug { get; set; }

		// 衝突中のコライダー
		protected List<ICollider> _collidedTargets = new List<ICollider>();
		
		public override void Update () {
			foreach (var target in Targets) {
				if (Detect(target)) {
					// 衝突していた場合
					if (_collidedTargets.Contains(target)) {
						// 前フレームでぶつかっていたらStay
						onCollisionStay?.Invoke(target);
					}
					else {
						// 前フレームでぶつかっていなかったらEnter
						onCollisionEnter?.Invoke(target);
						_collidedTargets.Add(target);
					}
				}
				else {
					// 衝突していなかった場合
					if (_collidedTargets.Contains(target)) {
						// 前フレームでぶつかっていたらExit
						onCollisionExit?.Invoke(target);
						_collidedTargets.Remove(target);
					}
				}
			}

			// デバッグ用
			if (IsDebug) {
				if (_collidedTargets.Count() >= 1) {
					var material = GameObject.GetComponent<IRenderer>().Material;
					material.SetColor("_Tint", new Color4(1,0,0));
				}
				else {
					var material = GameObject.GetComponent<IRenderer>().Material;
					material.SetColor("_Tint", new Color4(1,1,1));
				}
			}

		}

		protected abstract bool Detect(ICollider target);
	}
}
