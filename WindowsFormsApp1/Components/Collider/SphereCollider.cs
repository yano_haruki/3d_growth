﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	class SphereCollider : Component, ICollider, ISphere
	{
		public Vector3 Center { get; set; }
		private float _radius = 0.5f;
		public float Radius { get { return _radius * Transform.LocalScale.GetMaxElement(); } set { _radius = value; } }
    
		public Vector3 WorldCenter { get { return Transform.Position + Center; } }

		/// <summary>
		/// 球との当たり判定
		/// </summary>
		public bool CheckSphere(ISphere collider)
		{
			var collideDistance = Radius + collider.Radius;
			return (WorldCenter - collider.WorldCenter).LengthSquared() <= collideDistance * collideDistance;
		}

		/// <summary>
		/// カプセルとの当たり判定
		/// </summary>
		public bool CheckCapsule(ICapsule collider)
		{
			throw new NotImplementedException();
		}
		
		/// <summary>
		/// 直方体との当たり判定
		/// </summary>
		public bool CheckAxisAlignedBoundingBox(IAxisAlignedBoundngBox collider)
		{
			throw new NotImplementedException();
		}

		public override void OnDrawGizmos()
		{
			base.OnDrawGizmos();
		}
	}
}
