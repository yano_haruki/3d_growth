﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	class CapsuleCollisionDetector : CollisionDetectorBase, ICapsule
	{
		public Vector3 Center { get; set; } = Vector3.Zero;
		private float _radius = 0.5f;
		public float Radius { get { return _radius * Transform.LocalScale.GetMaxElement(); } set { _radius = value; } }
		public float Height { get; } = 1.0f;
		public Vector3 StartPoint { get { return Center - Vector3.UnitY * Height * 0.5f; } }
		public Vector3 EndPoint { get { return Center + Vector3.UnitY * Height * 0.5f; } }
		public Vector3 WorldStartPoint { get { return Vector3.Transform(StartPoint, Transform.LocalToWorldMatrix).ToVector3(); } }
		public Vector3 WorldEndPoint { get { return Vector3.Transform(EndPoint, Transform.LocalToWorldMatrix).ToVector3(); } }

		protected override bool Detect(ICollider target)
		{
			return target.CheckCapsule(this);
		}
	}
}
