﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	class SphereCollisionDetector : CollisionDetectorBase, ISphere
	{
		public Vector3 Center { get; set; }
		private float _radius = 0.5f;
		public float Radius { get { return _radius * Transform.LocalScale.GetMaxElement(); } set { _radius = value; } }
		public Vector3 WorldCenter { get { return Transform.Position + Center; } }

		protected override bool Detect(ICollider target)
		{
			return target.CheckSphere(this);
		}
	}
}
