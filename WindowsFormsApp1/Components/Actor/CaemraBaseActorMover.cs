﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SlimDX;

namespace WindowsFormsApp1
{
	class CameraBaseActorMover : Component
	{
		private const float MOVE_SPEED_FACTOR	= 0.02f;
		private Transform _cameraTransform;

		public void Setup(Transform cameraTransform)
		{
			_cameraTransform = cameraTransform;
		}

		public override void Update()
		{
			if (Input.Instance.GetKey(Keys.Left)) {
				// 左に移動
				Transform.LocalPosition -= _cameraTransform.Right * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetKey(Keys.Right)) {
				// 右に移動
				Transform.LocalPosition += _cameraTransform.Right * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetKey(Keys.Up)) {
				// 上に移動
				Transform.LocalPosition += _cameraTransform.Up * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetKey(Keys.Down)) {
				// 下に移動
				Transform.LocalPosition -= _cameraTransform.Up * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetKey(Keys.E)) {
				// 前に移動
				Transform.LocalPosition += _cameraTransform.Forward * MOVE_SPEED_FACTOR;
			}
			if (Input.Instance.GetKey(Keys.Q)) {
				// 後ろに移動
				Transform.LocalPosition -= _cameraTransform.Forward * MOVE_SPEED_FACTOR;
			}
		}
	}
}
