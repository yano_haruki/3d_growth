﻿using SlimDX;

namespace WindowsFormsApp1
{
	//TODO とりあえずパフォーマンスは後回し、必要であればやる
	public class Transform : Component
	{
		public Vector3 Forward { get { return Vector3.TransformNormal(new Vector3(0, 0, 1), LocalToWorldMatrix); } }
		public Vector3 Back { get { return Vector3.TransformNormal(new Vector3(0, 0, -1), LocalToWorldMatrix); } }
		public Vector3 Up { get { return Vector3.TransformNormal(new Vector3(0, 1, 0), LocalToWorldMatrix); } }
		public Vector3 Down { get { return Vector3.TransformNormal(new Vector3(0, -1, 0), LocalToWorldMatrix); } }
		public Vector3 Right { get { return Vector3.TransformNormal(new Vector3(1, 0, 0), LocalToWorldMatrix); } }
		public Vector3 Left { get { return Vector3.TransformNormal(new Vector3(-1, 0, 0), LocalToWorldMatrix); } }
		
		// 位置
		public Vector3 LocalPosition { get; set; } = Vector3.Zero;
		public Vector3 Position => Vector3.Transform(Vector3.Zero, LocalToWorldMatrix).ToVector3();

		// 回転
		public Vector3 LocalEulerAngles { get; set; } = Vector3.Zero;
		public Quaternion LocalRotation 
		{
			get {
				// ZXYの順で回転を適用する
				return Quaternion.RotationAxis(Vector3.UnitZ, LocalEulerAngles.Z)
					* Quaternion.RotationAxis(Vector3.UnitX, LocalEulerAngles.X)
					* Quaternion.RotationAxis(Vector3.UnitY, LocalEulerAngles.Y);
			}
		}
		public Quaternion Rotation => (Parent == null ? Parent.Rotation : Quaternion.Identity) * LocalRotation;

		// スケール
		public Vector3 LocalScale { get; set; } = new Vector3(1, 1, 1);
		public Vector3 LossyScale => (Parent != null ? Parent.LossyScale : new Vector3(1, 1, 1)).Multiply(LocalScale);		
	
		// 親
		public Transform Parent { get; set; }
	
		public Matrix LocalToParentMatrix {
			get {
				return Matrix.Multiply
				(
					Matrix.Multiply
					(
						Matrix.Scaling(LocalScale),
						Matrix.RotationQuaternion(LocalRotation)
					),
					Matrix.Translation(LocalPosition)
				);
			}
		}
	
		public Matrix LocalToWorldMatrix => LocalToParentMatrix * (Parent == null ? Matrix.Identity : Parent.LocalToWorldMatrix);
	}
}