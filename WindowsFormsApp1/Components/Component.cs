﻿using System;

namespace WindowsFormsApp1
{
	public class Component
	{
		public GameObject GameObject { get; set; }
		public Transform Transform { get; set; }

		public void Initialize()
		{
			ComponentManager.Instance.Add(this);
		}

		public virtual void Update()
		{

		}

		public virtual void OnDrawGizmos()
		{

		}

		public void Dispose()
		{
			ComponentManager.Instance.Remove(this);
		}
	}
}