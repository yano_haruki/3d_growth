﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	public class Physics
	{
		/*
		public static bool Raycast(Vector3 origin, Vector3 direction, out ICollider collider)
		{
			var colliders = ComponentManager.Instance.GetComponents<ICollider>();
		}
		*/

		public static bool RaycastSphere(Vector3 origin, Vector3 direction, out ICollider collider)
		{
			var colliders = ComponentManager.Instance.GetComponents<SphereCollider>();
			foreach (var col in colliders) {
				if (Math3D.LineSphereIntersection(origin, direction, col.Center, col.Radius)) {
					collider = col;
					return true;
				}
			}
			collider = null;
			return false;
		}
	}
}
