﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;

namespace WindowsFormsApp1
{
	class Gizmos
	{
		public static List<IRenderer> Renderers { get; private set; } = new List<IRenderer>();

		public static void DrawSphere(Vector3 position, float radius)
		{			
			var renderer			= new GameObject().AddComponent<MeshRenderer>();
			renderer.Mesh			= Mesh.CreateSphere();
			renderer.Transform.LocalPosition = position;
			renderer.Material		= new Material(new Shader(Properties.Resources.shader_gizmo_outline));
			Renderers.Add(renderer);
		}

		public static void ClearRenderer()
		{
			foreach (var renderer in Renderers) {
				var component = renderer as Component;
				component.GameObject.Dispose();
			}
			Renderers.Clear();
		}
	}
}
