﻿using SlimDX;

namespace WindowsFormsApp1
{
	public class DirectionalLight : Component
	{
		public Color4 Color { get; set; }	= new Color4(1, 1, 1);
		public Color4 ClearColor { get; set; }		= new Color4(1.0f, 0.5f, 0.5f, 0.5f);

		public override void Update()
		{
			base.Update();
			// ディレクショナルライトのSHADER_PROPERTY_NAME_WORLD_LIGHT_POSは、ディレクショナルライトが向いているのと逆の方向として定義（Unityと同じ）
			var direction			= Transform.Forward * -1;
			Shader.SetGlobalVector(Defines.SHADER_PROPERTY_NAME_WORLD_LIGHT_POS, new Vector4(direction, 0)); // ディレクショナルライトの場合w値には0を入れる
			Shader.SetGlobalColor(Defines.SHADER_PROPERTY_NAME_LIGHT_COLOR, Color);
		}

		public void DrawShadow()
		{
			Graphics.SetShadowMatrixVP(GetMatrixVP());

			// Set Viewport
			Graphics.SetViewport(true);

			// Clear
			Graphics.Clear(ClearColor, true);

			// DrawShadow
			var renderers		= ComponentManager
				.Instance
				.GetComponents<IShadowRenderer>();
			foreach (var renderer in renderers) {
				renderer.DrawShadow();
			}
		}

		public virtual Matrix GetMatrixVP()
		{
			var lightDir = Transform.Forward;
			//http://www.richardssoftware.net/2013/10/shadow-mapping-with-slimdx-and-directx.html
			// シーン情報は適当だけどとりあえずこれで・・
			var sceneCenter = Vector3.Zero; // 中心はワールドの中心
			var sceneRadius = 30.0f; // 実際はカメラの移す範囲で変えないと・・
			var lightPos = -2.0f * sceneRadius * lightDir;
			var targetPos = sceneCenter;
			var up = Vector3.UnitY;

			var v = Matrix.LookAtLH(lightPos, targetPos, up);

			var sphereCenterLS = Vector3.TransformCoordinate(targetPos, v);

			var l = sphereCenterLS.X - sceneRadius;
			var b = sphereCenterLS.Y - sceneRadius;
			var n = sphereCenterLS.Z - sceneRadius;
			var r = sphereCenterLS.X + sceneRadius;
			var t = sphereCenterLS.Y + sceneRadius;
			var f = sphereCenterLS.Z + sceneRadius;

			var p = Matrix.OrthoOffCenterLH(l, r, b, t, n, f);
			var T = new Matrix {
				M11 = 0.5f, M22 = -0.5f, M33 = 1.0f, M41 = 0.5f, M42 = 0.5f, M44 = 1.0f
			};
			var s = v * p * T;

			var view = v;
			var proj = p;
			var shadowTrans = s;

			// ホントはここでやるべきじゃないが一旦
			Shader.SetGlobalMatrix("_ShadowTransform", shadowTrans);

			return view * proj;


			// Perspectiveカメラと同じもの
			// テスト用
			/*
			float Fov = 60 * Mathf.Deg2Rad;
			float ZNear = 0.1f;
			float ZFar = 1000.0f;
			var view = Matrix.LookAtLH(Transform.Position, Transform.Position + Transform.Forward, Transform.Up);
			var proj =  Matrix.PerspectiveFovLH(Fov, (float)ScreenUtility.Width / (float)ScreenUtility.Height, ZNear, ZFar);
			return view * proj;
			*/
		}
	}
}
