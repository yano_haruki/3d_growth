﻿using System.Windows.Forms;
using SlimDX;
using SlimDX.Windows;
using System.Collections.Generic;
using SlimDX.DirectInput;

namespace WindowsFormsApp1
{
	public partial class GameForm : Form
	{
		// フレームレート
		private double _frameRate		= 30;
		// 前回Update時のTick
		private long _previousUpdateTick;
		// 現在のTickCount
		private long _currentTick;
		// 次回処理をする時点のTickCount
		private long _nextTick;

		private MyGame _game;

		public GameForm()
		{
			InitializeComponent();
		}

		public void Run()
		{
			_currentTick			= System.Environment.TickCount;
			_nextTick				= _currentTick;
			_previousUpdateTick		= _currentTick;
			
			// Graphicsを初期化
			FormBorderStyle					= FormBorderStyle.FixedSingle;
			ClientSizeChanged += (object sender, System.EventArgs e) => {
				ScreenUtility.Width		= ClientSize.Width;
				ScreenUtility.Height	= ClientSize.Height;
			};
			ScreenUtility.Width			= ClientSize.Width;
			ScreenUtility.Height		= ClientSize.Height;
			Graphics.InitDevice(ClientSize.Width, ClientSize.Height, Handle);
			
			// Gameを初期化
			_game			= new MyGame();
			_game.Setup();

			// Input
			Input.Instance.Setup();

			MessagePump.Run(this, MainLoop);
			
			Graphics.DisposeDevice();
		}

		private void MainLoop()
		{
 
			_currentTick			= System.Environment.TickCount;
			var diffMillis			= System.Math.Floor(1000.0 / _frameRate);

			if (_currentTick < _nextTick)
			{
				// まだ処理する時間に達していなかったら待つ
				System.Threading.Thread.Sleep((int)(_nextTick - _currentTick));
				return;
			}
			
			var delta				= _currentTick - _previousUpdateTick;
			Time.DeltaTime			= delta / 1000.0f;
			_previousUpdateTick		= _currentTick;

			// Update
			_game.Update();

			// LateUpdate
			Input.Instance.LateUpdate();
			var clientCursorPoint = PointToClient(Cursor.Position);
			Input.Instance.ScreenMousePosition = new Vector2(clientCursorPoint.X, clientCursorPoint.Y);
			_game.LateUpdate();

			if (_currentTick >= _nextTick + diffMillis)
			{
				// 前フレームでの描画処理に時間が掛かり過ぎた場合はこのフレームでは処理しない
			}
			else {
				// 描画処理
				_game.Draw();
			}

			// 次回処理をする時点のTickCountを更新する
			while (_currentTick >= _nextTick)
			{
				_nextTick += (long)diffMillis;
			}
		}

		private void GameForm_Load(object sender, System.EventArgs e)
		{

		}
	}
}