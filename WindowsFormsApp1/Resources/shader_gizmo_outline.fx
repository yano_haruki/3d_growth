matrix MATRIX_VP;
matrix MATRIX_M;

SamplerState mySampler
{
};

struct VertexPositionColor
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};
 
VertexPositionColor MyVertexShader(VertexPositionColor input)
{
	input.Position	= mul(input.Position, mul(MATRIX_M, MATRIX_VP));
	return input;
}
 
float4 MyPixelShader(VertexPositionColor input) : SV_Target
{
	return float4(0, 1, 0, 1);
}

technique10 BaseTechnique
<
	string Queue			= "Opaque";
>
{
	pass BasePass
	<
		string BlendSrc			= "One";
		string BlendDst			= "Zero";
		string ColorMask		= "RGB";
	>
	{
		SetVertexShader( CompileShader( vs_5_0, MyVertexShader() ) );
		SetPixelShader( CompileShader( ps_5_0, MyPixelShader() ) );
	}
}