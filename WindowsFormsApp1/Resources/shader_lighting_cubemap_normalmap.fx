
matrix MATRIX_VP;
matrix MATRIX_M;
matrix _ShadowTransform;
float4 _LightColor;
float4 _WorldSpaceLightPos;
float4 _WorldSpaceCameraPos;

Texture2D _MainTex;
Texture2D _NormalMap;
float4 _Tint = (1,1,1,1);
float _Metalness;
float _IndirectDiffRefl;
float _Roughness;
TextureCube _CubeMap;
Texture2D _ShadowMap;

#define F0                            0.04f
#define INDIRECT_DIFF_TEX_MIP        8.5f
#define MAX_SHININESS                50.0f
#define SHININESS_POW                0.2f
#define DIRECT_SPEC_ATTEN_MIN        0.2f
#define MAX_MIP                        8.0f

SamplerState mySampler
{
};

SamplerComparisonState samShadow
{
	Filter = COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
	AddressU = BORDER;
	AddressV = BORDER;
	AddressW = BORDER;
	BorderColor = float4(0.0f, 0.0f, 0.0f, 0.0f);

	ComparisonFunc = LESS;
};

struct appdata
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
	float4 Tangent : TANGENT;
};

struct v2f
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : TEXCOORD1;
	float3 WorldPos : TEXCOORD2;
	float4 Tangent : TEXCOORD3;
	float3 Binormal : TEXCOORD4;

	float4 ShadowPosH : TEXCOORD5;
};

static const float SMAP_SIZE = 2048.0f;
static const float SMAP_DX = 1.0f / SMAP_SIZE;

float CalcShadowFactor(SamplerComparisonState samShadow, Texture2D shadowMap, float4 shadowPosH)
{
	// Complete projection by doing division by w.
	shadowPosH.xyz /= shadowPosH.w;

	// Depth in NDC space.
	float depth = shadowPosH.z;

	return shadowMap.SampleCmpLevelZero(samShadow, shadowPosH.xy, depth).r;

	/*
	// Texel size.
	const float dx = SMAP_DX;

	float percentLit = 0.0f;
	const float2 offsets[9] =
	{
	float2(-dx,  -dx), float2(0.0f,  -dx), float2(dx,  -dx),
	float2(-dx, 0.0f), float2(0.0f, 0.0f), float2(dx, 0.0f),
	float2(-dx,  +dx), float2(0.0f,  +dx), float2(dx,  +dx)
	};

	[unroll]
	for (int i = 0; i < 9; ++i)
	{
	percentLit += shadowMap.SampleCmpLevelZero(samShadow,
	shadowPosH.xy + offsets[i], depth).r;
	}

	return percentLit /= 9.0f;
	*/
}
 
v2f MyVertexShader(appdata i)
{
	v2f o = (v2f)0;
	float4 worldPos	= mul(i.Position, MATRIX_M);
	o.Position		= mul(worldPos, MATRIX_VP);
	o.TexCoord		= i.TexCoord;
	o.Color			= i.Color;
	//o.Normal = mul(float4(i.Normal, 0), MATRIX_M);
	o.WorldPos		= worldPos.xyz;

	o.Binormal = normalize(cross(i.Normal.xyz, i.Tangent.xyz) * i.Tangent.w);
	o.Normal = mul(i.Normal, MATRIX_M);
	o.Tangent = mul(i.Tangent.xyz, MATRIX_M);
	o.Binormal = mul(o.Binormal, MATRIX_M);

	// Generate projective tex-coords to project shadow map onto scene.
	o.ShadowPosH = mul(float4(i.Position.xyz, 1.0), mul(MATRIX_M, _ShadowTransform));

	return o;
}
 
float4 MyPixelShader(v2f i) : SV_Target
{
	float3 normal = _NormalMap.Sample(mySampler, i.TexCoord).xyz;
	normal = normal.xyz * 2.0 - 1.0;
	// ワールド空間に変換
	normal = (i.Tangent * normal.x) + (i.Binormal * normal.y) + (i.Normal * normal.z);

	//return float4(normal.x, normal.y, normal.z, 1);
//return i.Color;
	float3 viewDir	= normalize(_WorldSpaceCameraPos.xyz - i.WorldPos);
	float3 lightDir	= normalize(_WorldSpaceLightPos.w == 0 ? _WorldSpaceLightPos : _WorldSpaceLightPos - i.WorldPos);
	float3 HalfVec	= normalize(viewDir + lightDir);
	
	float4 baseColor = _MainTex.Sample(mySampler, i.TexCoord) * _Tint;	
	float3 specColor = lerp(1, baseColor.rgb, _Metalness);
	
	// direct diffuse
	float3 directDiff = baseColor * max(0, dot(_WorldSpaceLightPos.xyz, normal)) * _LightColor.rgb;

	// direct specular
	float shininess = lerp(MAX_SHININESS, 1.0, pow(_Roughness, SHININESS_POW));
	half directSpecAtten = lerp(DIRECT_SPEC_ATTEN_MIN, 1.0, shininess / MAX_SHININESS);
	half3 directSpec = pow(max(0.0, dot(normal, HalfVec)), shininess) * _LightColor.rgb * specColor * directSpecAtten;

	// indirect diff
	half fresnel = F0 + (1 - F0) * pow(1 - dot(viewDir, normal), 5);
	half indirectRefl = lerp(fresnel, 1, _Metalness);
	indirectRefl = clamp(indirectRefl, 0, 0.5);
	float3 reflDir = reflect(-viewDir, normal);
	half3 indirectDiff = baseColor * /*_CubeMap.Sample(mySampler, reflDir) * */_IndirectDiffRefl;

	// indirect spec
	half3 indirectSpec = _CubeMap.Sample(mySampler, reflDir) * specColor;

	// Shadow
	float shadow = CalcShadowFactor(samShadow, _ShadowMap, i.ShadowPosH);

	float4 col = 1;
	//return indirectRefl;
	//col.rgb = directDiff;
	//col.rgb = indirectSpec;
	//col.rgb = indirectDiff;
	//col.rgb = lerp(indirectDiff, indirectSpec, indirectRefl);
	col.rgb = lerp(directDiff, directSpec, _Metalness) + lerp(indirectDiff, indirectSpec, indirectRefl);
	col.rgb *= shadow;
	col.a = 1;
	return col;
}
 
technique10 BaseTechnique
<
	string Queue			= "Geometry";
>
{
	pass BasePass
	<
		string BlendSrc			= "One";
		string BlendDst			= "Zero";
		string ColorMask		= "RGB";
	>
	{
		SetVertexShader( CompileShader( vs_5_0, MyVertexShader() ) );
		SetPixelShader( CompileShader( ps_5_0, MyPixelShader() ) );
	}
}