
matrix MATRIX_VP;
matrix MATRIX_M;
float4 _LightColor;
float4 _WorldSpaceLightPos;
float4 _WorldSpaceCameraPos;

Texture2D _MainTex;
float4 _Tint = (1,1,1,1);

SamplerState mySampler
{
};

struct appdata
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};

struct v2f
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : TEXCOORD1;
};
 
v2f MyVertexShader(appdata i)
{
	v2f o = (v2f)0;
	float4 worldPos	= mul(i.Position, MATRIX_M);
	o.Position		= mul(worldPos, MATRIX_VP);
	o.TexCoord		= i.TexCoord;
	o.Color = i.Color;
	o.Normal = i.Normal;
	return o;
}
 
float4 MyPixelShader(v2f i) : SV_Target
{
	float4 color	= _MainTex.Sample(mySampler, i.TexCoord) * _Tint;
	//return float4(i.Normal, 1);
	return color;
}
 
technique10 BaseTechnique
<
	string Queue			= "Geometry";
>
{
	pass BasePass
	<
		string BlendSrc			= "One";
		string BlendDst			= "Zero";
		//string BlendSrc			= "SourceAlpha";
		//string BlendDst			= "InverseSourceAlpha";
		string ColorMask		= "RGBA";
	>
	{
		SetVertexShader( CompileShader( vs_5_0, MyVertexShader() ) );
		SetPixelShader( CompileShader( ps_5_0, MyPixelShader() ) );
	}
}