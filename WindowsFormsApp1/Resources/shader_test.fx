
matrix MATRIX_VP;
matrix MATRIX_M;
Texture2D _MainTex;
float4 _Tint = (1,1,1,1);
float4 _LightColor;
float4 _WorldSpaceLightPos;

SamplerState mySampler
{
};

struct appdata
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};

struct v2f
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};
 
v2f MyVertexShader(appdata i)
{
	v2f o = (v2f)0;
	o.Position	= mul(i.Position, mul(MATRIX_M, MATRIX_VP));
	o.TexCoord	= i.TexCoord;
	o.Color		= i.Color;
	o.Normal	= mul(float4(i.Normal, 0), MATRIX_M);
	return o;
}
 
float4 MyPixelShader(v2f i) : SV_Target
{
	i.Normal		= normalize(i.Normal);
	//return i.Color;
	return max(0, dot(_WorldSpaceLightPos.xyz, i.Normal));
	//return float4(i.Normal, 1);
	return _LightColor;
	return _MainTex.Sample(mySampler, i.TexCoord);
}
 
technique10 BaseTechnique
<
	string Queue			= "Geometry";
>
{
	pass BasePass
	<
		string BlendSrc			= "One";
		string BlendDst			= "Zero";
		string ColorMask		= "RGBA";
	>
	{
		SetVertexShader( CompileShader( vs_5_0, MyVertexShader() ) );
		SetPixelShader( CompileShader( ps_5_0, MyPixelShader() ) );
	}
}