matrix MATRIX_VP;
matrix MATRIX_M;
Texture2D _MainTex;
float4 _Tint = (1,1,1,1);

SamplerState mySampler
{
};

struct VertexPositionColor
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};
 
VertexPositionColor MyVertexShader(VertexPositionColor input)
{
	input.Position	= mul(input.Position, mul(MATRIX_M, MATRIX_VP));
	return input;
}
 
float4 MyPixelShader(VertexPositionColor input) : SV_Target
{
	half4 color			= _MainTex.Sample(mySampler, input.TexCoord) * _Tint;
	color.rgb			*= input.Color.rgb;
	return color;
}

technique10 BaseTechnique
<
	string Queue			= "Transparent";
>
{
	pass BasePass
	<
		// アルファブレンド
		string BlendSrc			= "SourceAlpha";
		string BlendDst			= "InverseSourceAlpha";
		// RGBだけ書き込む
		string ColorMask		= "RGB";
	>
	{
		SetVertexShader( CompileShader( vs_5_0, MyVertexShader() ) );
		SetPixelShader( CompileShader( ps_5_0, MyPixelShader() ) );
	}
}