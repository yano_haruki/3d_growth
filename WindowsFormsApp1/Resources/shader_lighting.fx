
matrix MATRIX_VP;
matrix MATRIX_M;
float4 _LightColor;
float4 _WorldSpaceLightPos;
float4 _WorldSpaceCameraPos;

Texture2D _MainTex;
float4 _Tint = (1,1,1,1);
float4 _AmbientColor;
float _Shininess;

SamplerState mySampler
{
};

struct appdata
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};

struct v2f
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : TEXCOORD1;
	float3 WorldPos : TEXCOORD2;
};
 
v2f MyVertexShader(appdata i)
{
	v2f o = (v2f)0;
	float4 worldPos	= mul(i.Position, MATRIX_M);
	o.Position		= mul(worldPos, MATRIX_VP);
	o.TexCoord		= i.TexCoord;
	o.Color			= i.Color;
	o.Normal		= mul(float4(i.Normal, 0), MATRIX_M);
	o.WorldPos		= worldPos.xyz;
	return o;
}
 
float4 MyPixelShader(v2f i) : SV_Target
{
	i.Normal		= normalize(i.Normal);
	float3 viewDir	= normalize(_WorldSpaceCameraPos.xyz - i.WorldPos);
	float3 lightDir	= normalize(_WorldSpaceLightPos.w == 0 ? _WorldSpaceLightPos : _WorldSpaceLightPos - i.WorldPos);
	float3 HalfVec	= normalize(viewDir + lightDir);

	// diffuse
	float4 color	= _MainTex.Sample(mySampler, i.TexCoord) * _Tint;
	float diffuse	= max(0, dot(_WorldSpaceLightPos.xyz, i.Normal));
	
	// Specular
	float specular	= pow(max(0, dot(HalfVec, i.Normal)), _Shininess);
	color.rgb		*= diffuse * _LightColor.rgb + _AmbientColor.rgb + specular;
	return saturate(color);
}
 
technique10 BaseTechnique
<
	string Queue			= "Transparent";
>
{
	pass BasePass
	<
		//string BlendSrc			= "One";
		//string BlendDst			= "Zero";
		string BlendSrc			= "SourceAlpha";
		string BlendDst			= "InverseSourceAlpha";
		string ColorMask		= "RGBA";
	>
	{
		SetVertexShader( CompileShader( vs_5_0, MyVertexShader() ) );
		SetPixelShader( CompileShader( ps_5_0, MyPixelShader() ) );
	}
}