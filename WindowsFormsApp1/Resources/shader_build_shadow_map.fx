
matrix MATRIX_VP;
matrix MATRIX_M;

Texture2D _MainTex;

SamplerState mySampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

struct appdata
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};

struct v2f
{
	float4 Position : SV_Position;
	float2 TexCoord : TEXCOORD;
};
 
v2f MyVertexShader(appdata i)
{
	v2f o = (v2f)0;
	float4 worldPos	= mul(i.Position, MATRIX_M);
	o.Position		= mul(worldPos, MATRIX_VP);
	o.TexCoord		= i.TexCoord;
	return o;
}
 
float4 MyPixelShader(v2f i) : SV_Target
{
	float4 baseColor = _MainTex.Sample(mySampler, i.TexCoord);
	// Don't write transparent pixels to the shadow map.
	clip(baseColor.a - 0.15f);
	return baseColor;
}

RasterizerState Depth
{
	DepthBias = 10000;
	DepthBiasClamp = 0.0f;
	SlopeScaledDepthBias = 1.0f;
};

technique10 BaseTechnique
<
	string Queue			= "Depth";
>
{
	pass BasePass
	<
		string BlendSrc			= "One";
		string BlendDst			= "Zero";
		string ColorMask		= "RGB";
	>
	{
		SetVertexShader( CompileShader( vs_5_0, MyVertexShader() ) );
		SetGeometryShader(NULL);
		SetPixelShader(NULL);
		SetRasterizerState(Depth);
	}
}