matrix MATRIX_VP;
matrix MATRIX_M;
float4 _LightColor;
float4 _WorldSpaceLightPos;
float4 _WorldSpaceCameraPos;

TextureCube _CubeMap;

SamplerState mySampler
{
};

struct appdata
{
	float4 Position : SV_Position;
	float4 Color : COLOR;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
};

struct v2f
{
	float4 Position : SV_Position;
	float3 Normal : TEXCOORD1;
};
 
v2f MyVertexShader(appdata i)
{
	v2f o = (v2f)0;
	float4 worldPos	= mul(i.Position, MATRIX_M);
	o.Position		= mul(worldPos, MATRIX_VP);
	o.Normal		= mul(float4(i.Normal, 0), MATRIX_M);
	return o;
}
 
float4 MyPixelShader(v2f i) : SV_Target
{
	half3 dir = i.Normal;
	return _CubeMap.Sample(mySampler, dir);
}

RasterizerState NoCull
{
    CullMode = None;
};

DepthStencilState LessEqualDSS
{
    // Make sure the depth function is LESS_EQUAL and not just LESS.  
    // Otherwise, the normalized depth values at z = 1 (NDC) will 
    // fail the depth test if the depth buffer was cleared to 1.
    DepthFunc = LESS_EQUAL;
};
 
technique10 BaseTechnique
<
	string Queue			= "Skybox";
>
{
	pass BasePass
	<
		string BlendSrc			= "One";
		string BlendDst			= "Zero";
		string ColorMask		= "RGB";
	>
	{
		SetVertexShader( CompileShader( vs_5_0, MyVertexShader() ) );
		SetPixelShader( CompileShader( ps_5_0, MyPixelShader() ) );
		SetRasterizerState(NoCull);
		SetDepthStencilState(LessEqualDSS, 0);
	}
}